import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/simulacro_model.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_markdown/flutter_markdown.dart';
// import 'package:flutter_html_widget/flutter_html_widget.dart';
// import 'package:flutter_html_view/flutter_html_view.dart';

class simulacrosPageView extends StatefulWidget {
  final SimulacroModel simulacro;
  final Function siguientePagina;

  simulacrosPageView(
      {@required this.simulacro, @required this.siguientePagina});

  @override
  _simulacrosPageViewState createState() => _simulacrosPageViewState();
}

class _simulacrosPageViewState extends State<simulacrosPageView> {
  // int _currentIndex=0;
  PageController _pageController;
  int respuestaSeleccionada;
  int index=0;
  int temp;

  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    index=0;
    temp = 1;
    _pageController =
      new PageController(initialPage: index, viewportFraction: 1, keepPage: true);
  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _pageController.dispose();
  }
  void _nextPage(int delta) {
    _pageController.animateToPage(
        index + delta,
        duration: const Duration(milliseconds: 300),
        curve: Curves.ease
    );
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
 
    return Column(
      
      children: <Widget>[
        SizedBox(height: size.height*0.003,),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            IconButton(
              icon: Icon(
                Icons.navigate_before,
                size: 30,
                ),
              onPressed: (){

              },
            ),
            Container(
              child: Text(
                '$temp',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18
                ),
              ),
              color: Colors.red,
              width: size.width*0.07,
              alignment: Alignment.center,
            ),
            IconButton(
              icon: Icon(
                Icons.navigate_next,
                size: 30,
              ),
              onPressed: (){
                
              },
            ),
          ],
        ),
        Container(
          height: size.height * 1,
          child: PageView.builder(
            physics: BouncingScrollPhysics(),
            itemBuilder:(BuildContext context, int indx){
              return _cards(context, widget.simulacro.questions[indx]);
            },
            // pageSnapping: true,
            controller: _pageController,
            onPageChanged: (page){
              setState(() {
                index=page;
                temp = index+1;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _cards(BuildContext context, Question pregunta) {
    // _currentIndex=i+1;
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);
    
    
    String markdown = html2md.convert(pregunta.content);
    return Column(
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          // SizedBox(height: size.height*0.025,),
        
          Container(
              padding: EdgeInsets.all(size.height * 0.02),
              alignment: Alignment.center,
              child: new MarkdownBody(
                data: markdown.replaceAll('&nbsp;', ' ').replaceAll('**', '').replaceAll('&amp', '&').replaceAll('&quot', ''),               
                // styleSheet: MarkdownStyleSheet(
                // ),
              )
          ),
          SizedBox(height: size.height*0.02,),
          Column(
            children: _options(context, pregunta, bloc),
          )
          ,SizedBox(height: size.height*0.04,),
        ],
      );
    
  }

  List<Widget> _options(
      BuildContext context, Question options, LoginBloc bloc) {
    int x = 0;

    return options.answers.map((option) {
      x++;
      String markdown = html2md.convert(option.content);
      return RadioListTile(
          value: int.parse(option.id),
          title:new MarkdownBody(
                data: markdown.replaceAll('&nbsp;', ' '),               
                // styleSheet: MarkdownStyleSheet(
                // ),
              ),
          //  Text(
          //   option.content.replaceAll('<sub>', '').replaceAll('</sub>', '').replaceAll('<p>', '').replaceAll('</p>', '').replaceAll('<br>', '').replaceAll('<div>', '').replaceAll('</div>', '').,
          // ),
          groupValue: options.selectedAnswer,
          activeColor: Color(bloc.faction.back_color),
          onChanged: (int value) {
            setState(() {
              options.selectedAnswer = value;
            });
          });
    }).toList();
  }
}
