import 'dart:async';

import 'package:rxdart/rxdart.dart';


class AuxBloc {

  final _contraRelojController    = BehaviorSubject<dynamic>();
  final _pointsController   = BehaviorSubject<dynamic>();

  //Recuperar datos del Stream
  get contraRelojStream    => _contraRelojController.stream;
  get pointStream => _pointsController.stream;

  //Obtener el último valor insertado al Stream
  Stream get contraReloj    => _contraRelojController.value;
  Stream get points => _pointsController.value;


  dispose(){
    _contraRelojController?.close();
    _pointsController?.close();
  }





}