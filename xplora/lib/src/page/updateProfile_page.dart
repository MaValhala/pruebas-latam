import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/profile_provider.dart';
import 'package:xplora/src/utils/utils.dart';

class UpdateProfile extends StatefulWidget {
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

final profileProvider = new ProfileProvider();

class _UpdateProfileState extends State<UpdateProfile> {
  List<String> _documentTypes = [
    // 'Tipo de Documento',
    'Cedula de Ciudadania',
    'Tarjeta de Identidad',
    'Cedula de Extranjeria',
    'Pasaporte',
    'Registro Civil',
  ];
  var _documentT2 = {
    '1':'Cedula de Ciudadania',
    '2':'Tarjeta de Identidad',
    '3':'Cedula de Extranjeria',
    '4':'Pasaporte',
    '5':'Registro Civil',
    'Cedula de Ciudadania' : 'Cedula de Ciudadania',
    'Tarjeta de Identidad' : 'Tarjeta de Identidad',
    'Cedula de Extranjeria' : 'Cedula de Extranjeria',
    'Pasaporte' : 'Pasaporte',
    'Registro Civil' :'Registro Civil'

  };

  var _documentT = {
    'Cedula de Ciudadania': '1',
    'Tarjeta de Identidad': '2',
    'Cedula de Extranjeria': '3',
    'Pasaporte': '4',
    'Registro Civil': '5'
  };

  String _name = '';
  String _surname = '';
  String _email = '';
  String _documentType = '';
  String _idDocument='';
  String _password = '';
  String _phone = '';
  GlobalKey<FormState> _key = GlobalKey();
  GlobalKey<FormState> _key2 = GlobalKey();
  RegExp emailRegExp =
      new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');

  _update(BuildContext context, LoginBloc bloc) async{
    // final bloc = ProviderLogin.of(context);
    try{
      loadings(context);
      Map info = await profileProvider.actualizarUsuario(_email, _password, _phone, _documentT[_documentType], _idDocument, bloc);
      if (info['status']){
        if (_email != ''){
          bloc.changeEmail(_email);
        }
        if (_phone != ''){
          bloc.changePhone(_phone);
        }
        if (_documentType != ''){
          bloc.changeDocumentType(_documentType);
        }
        if (_idDocument!=''){
          bloc.changeDocument(_idDocument);
        }
        // bloc.changeEmail(_email);
        Navigator.pop(context);
        Navigator.pop(context);
      }else{
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    }catch(e){

    }
  
  
  }
  _updatePasword(BuildContext context, LoginBloc bloc) async{
    // final bloc = ProviderLogin.of(context);
    Map info = await profileProvider.actualizarPassword(_password, bloc.pin);
    if (info['status']){
      bloc.changePassword(_password);
      Navigator.pop(context);
    }else{
      mostrarAlerta(context, info['mensaje']);
    }
  
  
  }



  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);

    

    
    return Stack(children: <Widget>[
      Image.asset(
        'assets/FondoApp.png',
        height: size.height,
        width: size.width,
        fit: BoxFit.cover,
      ),     
      Scaffold(
          backgroundColor: Colors.transparent,
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Hero(
                  // el tag logo ya esta usado
                  tag: 'logo2',
                  child: Container(
                    margin: EdgeInsets.only(top: size.height * 0.1),
                    width: size.width * 0.4,
                    child: Image.asset("assets/LatamColor.png"),
                  ),
                ),
                SizedBox(height: size.height * 0.045),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: size.width*0.16,),
                    Text(
                      'Correo electrónico',
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 14
                      ),
                      textAlign: TextAlign.start,
                   ),
                  ],
                ),
                SizedBox(height: size.height*0.012,),
                _correo(context, bloc),
                SizedBox(height: size.height * 0.02),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: size.width*0.16,),
                    Text(
                      'Contraseña',
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 14
                      ),
                      textAlign: TextAlign.start,
                   ),
                  ],
                ),
                SizedBox(height: size.height*0.012,),
                _contrasena(context),
                SizedBox(height: size.height * 0.02),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: size.width*0.16,),
                    Text(
                      'Celular',
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 14
                      ),
                      textAlign: TextAlign.start,
                   ),
                  ],
                ),
                SizedBox(height: size.height*0.012,),
                _cellphone(context, bloc),
                SizedBox(height: size.height * 0.02),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: size.width*0.16,),
                    Text(
                      'Tipo de documento',
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 14
                      ),
                      textAlign: TextAlign.start,
                   ),
                  ],
                ),
                SizedBox(height: size.height*0.012,),
                _tipoDocumento(context, bloc),
                SizedBox(height: size.height * 0.02),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(width: size.width*0.16,),
                    Text(
                      'Numero de documento',
                      style: TextStyle(
                        color: Colors.black54,
                        fontSize: 14
                      ),
                      textAlign: TextAlign.start,
                   ),
                  ],
                ),
                SizedBox(height: size.height*0.012,),
                _documento(context, bloc),
                SizedBox(height: size.height * 0.04),
                _submit(context, bloc),
              ],
            ),
          )),
    ]);
  }

  Widget _correo(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.06,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.1),
      padding: EdgeInsets.symmetric(horizontal: size.width * 0.04),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Color.fromRGBO(210, 180, 222, 0.3),
      ),
      child: Form(
        key: _key,

        child: TextFormField(
          
          validator: (value){

            if(!emailRegExp.hasMatch(value)){
              return 'Ingrese una dirección de correo valida';
            }else{
              return null;
            }
          },
          keyboardType: TextInputType.emailAddress,
          style: TextStyle(color: Colors.black54),
          cursorColor: Colors.black54,
          decoration: InputDecoration(
            hintText: bloc.email,
            border: OutlineInputBorder(borderSide: BorderSide.none),
            hintStyle: TextStyle(
              fontSize: 14.0,
              height: 0.8,
              color: Colors.black54,
            ),
          ),
          onChanged: (value) {
            setState(() {
              _email = value;
            });
          },
        ),
      ),
    );
  }

  Widget _contrasena(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.06,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.1),
      padding: EdgeInsets.only(left: size.width * 0.04),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Color.fromRGBO(210, 180, 222, 0.3),
      ),
      child: TextFormField(
          keyboardType: TextInputType.text,
          
          obscureText: true,
          style: TextStyle(color: Colors.black54),
          cursorColor: Colors.black54,
          decoration: InputDecoration(
            hintText: 'Actualiza tu contraseña',
            border: OutlineInputBorder(borderSide: BorderSide.none),
            hintStyle: TextStyle(
              fontSize: 14.0,
              height: 0.8,
              color: Colors.black54
            ),
          ),
          onChanged: (value) {
            setState(() {
              _password = value;
            });
          },
        
      )
    );
  }

  Widget _cellphone(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.06,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.1),
      padding: EdgeInsets.only(left: size.width * 0.04),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Color.fromRGBO(210, 180, 222, 0.3),
      ),
      child: TextField(
        keyboardType: TextInputType.phone,
        style: TextStyle(color: Colors.black54),
        cursorColor: Colors.black54,
        decoration: InputDecoration(
          hintText: bloc.phone,
          border: OutlineInputBorder(borderSide: BorderSide.none),
          hintStyle: TextStyle(
            fontSize: 14.0,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          setState(() {
            _phone = value;
          });
        },
      ),
    );
  }

  // Widget _document(BuildContext context) {
  //   final size = MediaQuery.of(context).size;
  //   return Row(
  //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //     crossAxisAlignment: CrossAxisAlignment.end,
  //     children: <Widget>[
        
  //       SizedBox(width: size.width * 0.02),
        
  //     ],
  //   );
  // }

  Widget _tipoDocumento(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    
    List<DropdownMenuItem<String>> lista = new List();

    _documentTypes.forEach((type) {
      lista.add(DropdownMenuItem(child: Text(type), value: type));
    });
    return Column(
      
      children: <Widget>[
        Container(
          alignment: Alignment.center,
          height: size.height * 0.06,
          margin: EdgeInsets.symmetric(horizontal: size.width * 0.1),
          //padding: EdgeInsets.only(left: size.width * 0.3),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color: Color.fromRGBO(210, 180, 222, 0.3),
          ),
          child: DropdownButton(
            value: _documentType==''?_documentT2[bloc.documentType]:_documentType,
            items: lista,
            style: Theme.of(context)
                .textTheme
                .subhead
                .apply(color: Colors.black54, fontSizeFactor: 0.8),
            onChanged: (opt) {
              setState(() {
                setState(() {
                  _documentType = opt;
                });
              });
            },
          ),
        )
      ],
    );
  }

  Widget _documento(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.06,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.1),
      padding: EdgeInsets.only(left: size.width * 0.04),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        color: Color.fromRGBO(210, 180, 222, 0.3),
      ),
      child: TextField(
        keyboardType: TextInputType.number,
        style: TextStyle(color: Colors.black54),
        cursorColor: Colors.black54,
        decoration: InputDecoration(
          border: OutlineInputBorder(borderSide: BorderSide.none),
          hintText: bloc.document,
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          setState(() {
            _idDocument = value;
          });
        },
      ),
    );
  }

  Widget _submit(BuildContext context, LoginBloc bloc){
    final size = MediaQuery.of(context).size;
    return Container(
      // margin: EdgeInsets.only(left: size.width * 0.02),
      height: size.height*0.067,
      child: RaisedButton(
          //padding: EdgeInsets.symmetric(),
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            child: Text(
              'GUARDAR',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
                // color: Colors.white
              ),
            ),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
          elevation: 0.0,
          color: Color.fromRGBO(122, 43, 141, 1),
          textColor: Colors.white,
          disabledTextColor: Color.fromRGBO(122, 43, 141, 0.5),
          onPressed: () {
            // if(_email==null){
            //   _email= bloc.email;
            // }
            // if(_email==null){
            //   _email= bloc.email;
            // }
              if (_email==''&&_password==''&&_phone==''&&_documentType=='Tipo de Documento'&&_idDocument==''){
                Navigator.pop(context);
              }else{
                if (_email.length==0){
                
                  if(_password.length==0){
                    // mostrarAlerta3(context, 'Debes proporsionar una contraseña para continuar.', 'Contraseña no valida');
                    _update(context, bloc);
                  }else{
                    if(_password.length<6){
                      mostrarAlerta3(context, 'La contraseña debe tener mínimo seis caracteres.', 'Contraseña no valida');
                    }else{
                      _update(context, bloc);
                    }
                    
                    // _forgPass(context, _correo);
                  }
                }else{
                  if(_key.currentState.validate()){
                    _key.currentState.save();
                    print('hola');
                    if(_password.length==0){
                      // mostrarAlerta3(context, 'Debes proporsionar una contraseña para continuar.', 'Contraseña no valida');
                      _update(context, bloc);
                    }else{
                      if(_password.length<6){
                        mostrarAlerta3(context, 'La contraseña debe tener mínimo seis caracteres.', 'Contraseña no valida');
                      }else{
                        _update(context, bloc);
                      }
                      
                      // _forgPass(context, _correo);
                    }
                    // _forgPass(context, _correo);
                  }
                }
              }
              
            // Navigator.pushNamed(context, 'updateProfile');
          }),
    );
  }

  


}
