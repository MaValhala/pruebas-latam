import 'dart:convert';
import 'package:flutter/foundation.dart';

// RegistroModel registroModelFromJson(String str) => RegistroModel.fromJson(json.decode(str));

String registroModelToJson(RegistroModel data) => json.encode(data.toJson());

class GetData {

    List<RegistroModel> items = new List();

    GetData();

    GetData.fromJsonList( List<dynamic> jsonList ){
      if ( jsonList == null ) return;

      for ( var item in jsonList ){
        final location = new RegistroModel.fromJson( item );
        items.add( location );
      }
    }

}

class RegistroModel {
    String id;
    String tipoId;
    String valor;
    String date;
    String firstName;
    String surname;
    String firstLastname;
    String secondLastname;
    String email;
    String password;
    String cellphone;
    String gender;
    String originCountry;
    String originProvince;
    String originCity;
    String country;
    String province;
    String city;
    String postalCode;
    String estrato;
    String termsConditions;

    RegistroModel({
        this.id,
        this.tipoId,
        this.valor,
        this.date,
        this.firstName,
        this.surname,
        this.firstLastname,
        this.secondLastname,
        this.email,
        this.password,
        this.cellphone,
        this.gender,
        this.originCountry,
        this.originProvince,
        this.originCity,
        this.country,
        this.province,
        this.city,
        this.postalCode,
        this.estrato,
        this.termsConditions,
    });

    Map<String, String> toJson() => {
        "id"              : id,
        "tipo_id"         : tipoId,
        "valor"           : valor,
        "date"            : date,
        "first_name"      : firstName,
        "surname"         : surname,
        "first_lastname"  : firstLastname,
        "second_lastname" : secondLastname,
        "email"           : email,
        "password"        : password,
        "cellphone"       : cellphone,
        "gender"          : gender,
        "origin_country"  : originCountry,
        "origin_province" : originProvince,
        "origin_city"     : originCity,
        "country"         : country,
        "province"        : province,
        "city"            : city,
        "postal_code"     : postalCode,
        "estrato"         : estrato,
        "terms_conditions": termsConditions,
    };

    factory RegistroModel.fromJson(Map<String, dynamic> json) => RegistroModel(
        id              : json["id"],
        tipoId          : json["tipo_id"],
        valor           : json["valor"],
        date            : json["date"],
        firstName       : json["first_name"],
        surname         : json["surname"],
        firstLastname   : json["first_lastname"],
        secondLastname  : json["second_lastname"],
        email           : json["email"],
        password        : json["password"],
        cellphone       : json["cellphone"],
        gender          : json["gender"],
        originCountry   : json["origin_country"],
        originProvince  : json["origin_province"],
        originCity      : json["origin_city"],
        country         : json["country"],
        province        : json["province"],
        city            : json["city"],
        postalCode      : json["postal_code"],
        estrato         : json["estrato"],
        termsConditions : json["terms_conditions"],
    );



}





