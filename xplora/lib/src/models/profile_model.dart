import 'dart:convert';

String registroModelToJson(Profile data) => json.encode(data.toJson());

class Profile {
  final String firstName;
  final String secondName;
  final String firtsLastName;
  final String secondLastName;
  final int factionId;
  final int points;
  final int coins;

  Profile({
    this.firstName, 
    this.secondName, 
    this.firtsLastName, 
    this.secondLastName,
    this.factionId,
    this.points,
    this.coins
    });


  Map<String, dynamic> toJson() => {
    "firstName"       : firstName,
    "secondName"      : secondName,
    "firtsLastName"   : firtsLastName,
    "secondLastName"  : secondLastName,
    "factionId"       : factionId,
    "points"          : points,
    "coins"           : coins,
  };

  factory Profile.fromJson(Map<String, dynamic> json) => Profile(
      firstName       : json['firstName'],
      secondName      : json['secondName'],
      firtsLastName   : json['firtsLastName'],
      secondLastName  : json['secondLastName'],
      factionId       : json['factionId'],
      points          : json['points'],
      coins           : json['coins']
    );

}