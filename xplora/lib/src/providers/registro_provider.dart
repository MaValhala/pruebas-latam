import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:xplora/src/models/registro_model.dart';
import 'package:xplora/src/propierties/api_propierties.dart';

class RegistroProvider{

  String _apikey  ='';

  
  Future<String> _procesarRespuesta(Uri url, Map<String,String> query) async {
    final answer = await http.post(
      url,
      body: query,
    
    );
    final decodedAnswer = json.decode(answer.body);
    
    return decodedAnswer;
  }

  final String _url =  HOST;

  Future<Map<String, dynamic>> nuevoUsuario(String document, String documentNumber, String email, String name, String surname,String birthday, String lastname1,String lastname2,String gender, String city_resident ,String phone, String grade, String course, String password) async{
    print(birthday);
    final authData = {
      'user_document'       : document,
      'document_number'     : documentNumber,
      'email'               : email,
      'user_first_name'     : name,
      'user_second_name'    : surname,
      'user_birthday'       : birthday,
      'user_first_lastname' : lastname1,
      'user_second_lastname': lastname2,
      'gender'              : gender,
      'city_resident'       :city_resident,
      'grade'               :grade,
      'course'              : course,
      'cellphone'           : phone,


      'password'   : password,

    };

    print(authData);
    final resp = await http.post(
      '$_url/Auth/Register',
      body: authData 

    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );

    if ( decodedResp.containsKey('pin')){
      //TODO: Verifica si el usuario se puede loguear
      return { 

        'status': true,
        'coins' : decodedResp['coins'],
        'points': decodedResp['points'],
        'pin'   : decodedResp['pin']

      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }
  }

  Future<Map<dynamic, dynamic>> _procesarLocation(String url, Map data) async {
    try {
      switch (url) {
        case GET_COUNTRY_REGIST:
          final resp = await http.post(HOST + url);
          final Map decodedData = json.decode(resp.body);

          if (decodedData.containsKey('message')) {
            
            if (decodedData['message'] == 'success') {
              return decodedData;
            } else {
              return {"Error": "Error en la conexión"};
            }
          } else {
            return {"Error": "Error en la conexión"};
          }

          break;
        case GET_PROVINCE_REGIST:
          final resp = await http.post(HOST + url, body:(data));
          final Map decodedData = json.decode(resp.body);
          if (decodedData.containsKey('message')) {
            if (decodedData['message'] == 'success') {
              return decodedData;
            } else {
              return {"Error": "Error en la conexión"};
            }
          } else {
            return {"Error": "Error en la conexión"};
          }

          break;
        case GET_CITY_REGIST:
          final resp = await http.post(HOST + url, body:(data));
          // print(resp);
          final Map decodedData = json.decode(resp.body);
          // print(decodedData);
          if (decodedData.containsKey('message')) {
            if (decodedData['message'] == 'success') {
              return decodedData;
            } else {
              return {"Error": "Error en la conexión"};
            }
          } else {
            return {"Error": "Error en la conexión"};
          }

          break;
        default:
        return {"Error":"Error inesperado"};
      }
    } catch (e) {
      return {"Error": "Error en la conexión"};
    }
  }

  // .########.....###....####..######.
  // .##.....##...##.##....##..##....##
  // .##.....##..##...##...##..##......
  // .########..##.....##..##...######.
  // .##........#########..##........##
  // .##........##.....##..##..##....##
  // .##........##.....##.####..######.

  Future<List<dynamic>> getCountry() async {
    final Map temp = await _procesarLocation(GET_COUNTRY_REGIST,{});

    List items = new List();

    if (!temp.containsKey("Error")) {
      items = temp['countries'];
    } else {
      items = ["Error", "Error msj"];
    }

    return items;
  }

  // .########..########...#######..##.....##.####.##....##..######..########
  // .##.....##.##.....##.##.....##.##.....##..##..###...##.##....##.##......
  // .##.....##.##.....##.##.....##.##.....##..##..####..##.##.......##......
  // .########..########..##.....##.##.....##..##..##.##.##.##.......######..
  // .##........##...##...##.....##..##...##...##..##..####.##.......##......
  // .##........##....##..##.....##...##.##....##..##...###.##....##.##......
  // .##........##.....##..#######.....###....####.##....##..######..########

  Future<List<dynamic>> getProvince(String country) async {

    final parameters = new Map();
    parameters['country_id'] = country;

    final Map temp = await _procesarLocation(GET_PROVINCE_REGIST,parameters);
    

    List items = new List();

    if (!temp.containsKey("Error")) {
      items = temp['states'];
    } else {
      items = ["Error", "Error msj"];
    }

    return items;
  }

  Future<List<dynamic>> getCity(String province) async {

    final parameters = new Map();
    parameters['state_id'] = province;

    final Map temp = await _procesarLocation(GET_CITY_REGIST,parameters);
    

    List items = new List();

    if (!temp.containsKey("Error")) {
      items = temp['cities'];         //CAMBIAR EL "STATES" POR EL NOMBRE DEL LISTADO, MUY PROBABLEMENTE SEA "CITYS"
    } else {
      items = ["Error", "Error msj"];
    }

    return items;
  }

}