import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/profile_model.dart';
import 'package:xplora/src/propierties/api_propierties.dart';

class ProfileProvider {
  final String _url = HOST;

  Future<Profile> profile(String coins, String points) async {
    final resp = await http.get('$_url/User_data/find');

    if (resp.statusCode == 200) {
      // Si el servidor devuelve una repuesta OK, parseamos el JSON
      return Profile.fromJson(json.decode(resp.body));
    } else {
      // Si esta respuesta no fue OK, lanza un error.
      throw Exception('Failed to load post');
    }
  }

  Future<Map<String, dynamic>> actualizarCoins(String pin) async {
    final authData = {
      'pin': pin
    };

    final resp = await http.post('$_url/Student/get_coins', body: authData);

    Map<String, dynamic> decodedResp = json.decode(resp.body);
    if (decodedResp.containsKey('coins')) {
      //TODO: Verifica si el usuario se puede loguear
      return {
        'status': true,
        'coins' : decodedResp['coins'],
      };
    } else {
      return {'status': false, 'mensaje': decodedResp['error']};
    }
  }
  Future<Map<String, dynamic>> actualizarCoinsAfterAds(String pin) async {
    final authData = {
      'pin': pin
    };

    final resp = await http.post('$_url/Student/update_after_watching_advertising', body: authData);

    Map<String, dynamic> decodedResp = json.decode(resp.body);
    if (decodedResp.containsKey('coins')) {
      //TODO: Verifica si el usuario se puede loguear
      return {
        'status'    : true,
        'coins'     : decodedResp['coins'],
        'coins_won' : decodedResp['coins_won']
      };
    } else {
      return {'status': false, 'mensaje': decodedResp['error']};
    }
  }


  Future<Map<String, dynamic>> actualizarUsuario(String email, String password, String cellphone, String document, String documentNumber, LoginBloc bloc) async{
    var _documentT = {
      '1':'1',
      '2':'2',
      '3':'3',
      '4':'4',
      '5':'5',
      'Cedula de Ciudadania' : '1',
      'Tarjeta de Identidad' : '2',
      'Cedula de Extranjeria' : '3',
      'Pasaporte' : '4',
      'Registro Civil' :'5'

    };
    if (email == ''){
      email = bloc.email;
    }
    if (cellphone == ''){
      cellphone = bloc.phone;
    }
    if (document == 'Tipo de Documento'||document==null){
      document = _documentT[bloc.documentType];
    }
    if (documentNumber==''){
      documentNumber=bloc.document;
    }

    final authData = {
      'pin'             : bloc.pin,
      'email'           : email,
      'cellphone'       : cellphone,
      'type_document'   : document,
      'document'        : documentNumber,
      'password'        : password,
    };

    final resp = await http.post(
      '$_url/Student/update',
      body: authData 
    );

    Map<String, dynamic> decodedResp = json.decode(resp.body);

    if (decodedResp.containsKey('message')&&decodedResp['message']!='error'){
      return{
        
        'status' : true,
      };
    }else{
      return {'status': false, 'mensaje': decodedResp['error']};
    }

  }
  Future<Map<String, dynamic>> actualizarPassword(String password, String pin) async{

    final authData = {
      'pin'             : pin,
      'password'        : password,
    };

    final resp = await http.post(
      '$_url/Student/update_password',
      body: authData 
    );

    Map<String, dynamic> decodedResp = json.decode(resp.body);

    if (decodedResp.containsKey('message')&&decodedResp['message']!='error'){
      return{
        
        'status' : true,
      };
    }else{
      return {'status': false, 'mensaje': decodedResp['error']};
    }

  }
}



    // Map<String, dynamic> decodedResp = json.decode( resp.body );

    // if ( decodedResp.containsKey('pin')){
    //   //TODO: Verifica si el usuario se puede loguear
    //   return { 

    //     'status': true,
    //     'coins' : decodedResp['coins'],
    //     'points': decodedResp['points'],
    //     'pin'   : decodedResp['pin']

    //   };
    // } else {
    //   return { 'status': false, 'mensaje': decodedResp['error']};
    // }
