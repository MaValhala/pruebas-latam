import 'dart:convert';



class SimulacroModel {
    // String message;
    String simulacrumId;
    DateTime simulacrumMaxTime;
    List<Question> questions;
    var simulacroRespuestas = List<String>();

    SimulacroModel({
        // this.message,
        this.simulacrumId,
        this.simulacrumMaxTime,
        this.questions,
    });

    SimulacroModel.fromJson(Map<String, dynamic> json){
        simulacrumId      = json["simulacrum_id"];
        simulacrumMaxTime = DateTime.parse(json["simulacrum_max_time"]);
        questions         = List<Question>.from(json["questions"].map((x) => Question.fromJson(x)));
    }
    List<String> toJson() {
      for (var item in questions) {
        simulacroRespuestas.add(item.id);
        simulacroRespuestas.add(item.selectedAnswer.toString());
      }
      return simulacroRespuestas;
    }
}

class Question {
    String id;
    String content;
    List<Answer> answers;
    int selectedAnswer;

    Question({
        this.id,
        this.content,
        this.answers,
    });

    factory Question.fromJson(Map<String, dynamic> json) => Question(
        id      : json["id"],
        content : json["content"],
        answers : List<Answer>.from(json['answers']!=null?json["answers"].map((x) => Answer.fromJson(x))
                                              :null),
    );

    Map<String, String> toJson() => {
        "question_id": id,
        "answer_id"  :selectedAnswer.toString()
    };
    
}

class Answer {
    String id;
    String content;
    String questionId;
    DateTime createdAt;
    dynamic updatedAt;

    Answer({
        this.id,
        this.content,
        this.questionId,
        this.createdAt,
        this.updatedAt,
    });

    factory Answer.fromJson(Map<String, dynamic> json) => Answer(
        id: json["id"],
        content: json["content"],


    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "content": content,

    };
}