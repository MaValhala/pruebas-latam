import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/registro_model.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/registro_provider.dart';
import 'package:xplora/src/utils/utils.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:webview_flutter/webview_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class RegistroPage extends StatefulWidget {
  const RegistroPage({Key key}) : super(key: key);

  @override
  _RegistroPageState createState() => _RegistroPageState();
}

class _RegistroPageState extends State<RegistroPage> {
  List<String> _documentTypes = [
    'Tipo de Documento',
    'Cedula de Ciudadania',
    'Tarjeta de Identidad',
    'Cedula de Extranjeria',
    'Pasaporte',
    'Registro Civil',
  ];
  List<String> _grades = [
    'Seleccione su grado',
    'Sexto',
    'Séptimo',
    'Octavo',
    'Noveno',
    'Décimo',
    'Undécimo',
    'Graduado'
  ];
  List<String> _genderTypes = ['Género', 'Masculino', 'Femenino'];
  var _documentT = {
    'Cedula de Ciudadania': '1',
    'Tarjeta de Identidad': '2',
    'Cedula de Extranjeria': '3',
    'Pasaporte': '4',
    'Registro Civil': '5'
  };
  var _genderT = {
    'Masculino': '1',
    'Femenino': '2',
  };

  List<RegistroModel> getData;
  _RegistroPageState({Key key, this.getData});

  final registroProvider = new RegistroProvider();

  String _direction = '';
  bool _agree =
      false; /**TODO: Variable para controlar si el usuario acepta o no los terminos y condiciones */
  String _last_name1 = '';
  String _last_name2 = '';
  String _name = '';
  String _surname = '';
  String _email = '';
  String _documentType = 'Tipo de Documento';
  String _idDocument;
  String _originCountry = 'Seleccione un pais';
  String _originProvince = 'Seleccione un departamento';
  String _originCity = 'Seleccione una ciudad';
  String _country = 'Seleccione un pais';
  String _province = 'Seleccione un departamento';
  String _city = 'Seleccione una ciudad';
  String _gender = 'Género';
  String _postal_code = '';
  String _grade = 'Seleccione su grado';
  String _course = '';
  String _password = '123456';
  String _phone = '';
  String _estrato = '';
  String _coins = '';
  String _points = '';
  String _pin = '';

  DateTime _birthDate = DateTime.now();
  bool _selectedDate = false;

  _register(BuildContext context, LoginBloc bloc) async {
    //  Map info = await registroProvider.nuevoUsuario(_idType, _idDocument, _birthDate.toString(), _name, _surname, _last_name1, _last_name2, _email, _password, "1","1", "1", "1", "1", _postal_code, _estrato, "11212", _direction, _grade, _course);
    try {
      loadings(context);
      Map info = await registroProvider.nuevoUsuario(
          _documentT[_documentType],
          _idDocument,
          bloc.email,
          _name,
          _surname,
          DateFormat('d MMMM yyyy').format(_birthDate),
          _last_name1,
          _last_name2,
          _genderT[_gender],
          // _originCity,
          _city,
          // _direction,
          // _postal_code,
          // _estrato,
          _phone,
          _grade,
          _course,
          bloc.password);
      if (info['status']) {
        _coins = info['coins'];
        _points = info['points'];
        _pin = info['pin'];
        if (_coins != null) {
          bloc.setCoins(_coins);
          bloc.setPoints(_points);
          bloc.setPin(_pin);
          bloc.changeEmail(_email);
          bloc.changePassword(_password);
          bloc.changeName(_name);
          bloc.changeSurname(_surname);
          bloc.changeLastName1(_last_name1);
          bloc.changeLastName2(_last_name2);
          bloc.changePhone(_phone);
          Navigator.pushNamedAndRemoveUntil(
              context, 'factions', (Route<dynamic> route) => false);
          // Navigator.pushReplacementNamed(context, );
        }
        // Navigator.pushReplacementNamed(context, 'factions');
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    } catch (e) {}
  }

  final formKey = GlobalKey<FormState>();
  final format = DateFormat("d MMM yyyy");

  Future<Null> _birthDay(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _birthDate,
        firstDate: DateTime(1990, 8),
        lastDate: DateTime(2030),
        locale: Locale('es'));

    if (picked != null && picked != _birthDate)
      setState(() {
        _birthDate = picked;
        _selectedDate = true;
      });
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(size.height * 0.13),
          child: SafeArea(
            child: Container(
                alignment: Alignment.center,
                height: size.height * 0.13,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 5),
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                    fit: BoxFit.cover,
                  ),
                )),
          )),
      body: ListView(
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Column(
            children: <Widget>[
              _datos(context),
            ],
          ),
        ],
      ),
    );
  }

  Widget _datos(BuildContext context) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Container(
            width: size.width * 1,
            height: size.height * 0.07,
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
            margin: EdgeInsets.only(
              top: 20,
            ),
            color: Color.fromRGBO(122, 42, 139, 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: size.width * 0.02,
                ),
                Icon(
                  Icons.assignment_ind,
                  color: Colors.white,
                ),
                SizedBox(
                  width: size.width * 0.02,
                ),
                Text(
                  'NOMBRES',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                  ),
                ),
              ],
            )),
        _datosPersonales(context),
        Container(
            width: size.width * 1,
            height: size.height * 0.07,
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
            margin: EdgeInsets.only(
              top: 20,
            ),
            color: Color.fromRGBO(122, 42, 139, 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: size.width * 0.02,
                ),
                Icon(
                  Icons.contact_mail,
                  color: Colors.white,
                ),
                SizedBox(
                  width: size.width * 0.02,
                ),
                Text(
                  'DATOS PERSONALES',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                  ),
                ),
              ],
            )),
        _documentoIdentidad(context),
        Container(
            alignment: Alignment.center,
            width: size.width * 1,
            height: size.height * 0.07,
            // alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
            margin: EdgeInsets.only(
              top: 20,
            ),
            color: Color.fromRGBO(122, 42, 139, 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: size.width * 0.02,
                ),
                Icon(
                  Icons.phone,
                  color: Colors.white,
                ),
                SizedBox(
                  width: size.width * 0.02,
                ),
                Text(
                  'CONTACTO',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                  ),
                ),
              ],
            )),
        _datosContacto(context, bloc),
        // Container(
        //     width: size.width * 1,
        //     height: size.height * 0.07,
        //     alignment: Alignment.center,
        //     padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
        //     margin: EdgeInsets.only(
        //       top: 20,
        //     ),
        //     color: Color.fromRGBO(122, 42, 139, 1),
        //     child: Row(
        //       mainAxisAlignment: MainAxisAlignment.start,
        //       children: <Widget>[
        //         SizedBox(
        //           width: size.width * 0.02,
        //         ),
        //         Icon(Icons.calendar_today, color: Colors.white),
        //         SizedBox(
        //           width: size.width * 0.02,
        //         ),
        //         Text(
        //           'LUGAR DE NACIMIENTO',
        //           textAlign: TextAlign.center,
        //           style: TextStyle(
        //             color: Colors.white,
        //             fontWeight: FontWeight.w400,
        //             fontSize: 18,
        //           ),
        //         ),
        //       ],
        //     )),
        // _datosNacimiento(context),
        Container(
            width: size.width * 1,
            height: size.height * 0.07,
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
            margin: EdgeInsets.only(top: 20),
            color: Color.fromRGBO(122, 42, 139, 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: size.width * 0.02,
                ),
                Icon(
                  Icons.school,
                  color: Colors.white,
                ),
                SizedBox(
                  width: size.width * 0.02,
                ),
                Text(
                  'INFORMACIÓN ACADEMICA',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                  ),
                ),
              ],
            )),
        _datosAcademicos(context),
        // Container(
        //     alignment: Alignment.center,
        //     width: size.width * 1,
        //     height: size.height * 0.07,
        //     // alignment: Alignment.center,
        //     padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
        //     margin: EdgeInsets.only(
        //       top: 20,
        //     ),
        //     color: Color.fromRGBO(122, 42, 139, 1),
        //     child: Row(
        //       mainAxisAlignment: MainAxisAlignment.start,
        //       children: <Widget>[
        //         SizedBox(
        //           width: size.width * 0.02,
        //         ),
        //         Icon(
        //           Icons.person_add,
        //           color: Colors.white,
        //         ),
        //         SizedBox(
        //           width: size.width * 0.02,
        //         ),
        //         Text(
        //           'Adicionales',
        //           textAlign: TextAlign.center,
        //           style: TextStyle(
        //             color: Colors.white,
        //             fontWeight: FontWeight.w400,
        //             fontSize: 18,
        //           ),
        //         ),
        //       ],
        //     )),
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Row(
            children: <Widget>[
              Checkbox(
                value: _agree,
                checkColor: Colors.white,
                activeColor: Colors.purple,
                onChanged: (value) {
                  setState(() {
                    _agree = value;
                  });
                },
              ),
              GestureDetector(
                child: Text('Acepto los terminos y condiciones'),
                onTap: () {
                  // mostrarAlerta2(context);
                  // Builder(builder: (BuildContext contex){
                  //   return WebView(
                  //     initialUrl: 'https://pruebaslatam.com/Terminoscondiciones',
                  //     gestureNavigationEnabled: true,

                  //   );
                  // });
                  _launchURL();
                },
              )
            ],
          ),
        ),
        SizedBox(height: 20),
        _botones(context, bloc),
      ],
    );
  }

  _launchURL() async {
    const url = 'https://pruebaslatam.com/Terminoscondiciones';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'No hemos podido cargar los terminos y condiciones.';
    }
  }

  Widget _documentoIdentidad(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        //physics: const NeverScrollableScrollPhysics(), //*Evita el scroll de una pagina*//
        child: Column(
      children: <Widget>[
        Container(
          width: size.width * 0.8,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  _tipoDocumento(context),
                  SizedBox(width: size.width * 0.06),
                  _numeroDocumento(context),
                ],
              ),
              SizedBox(height: 18),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  _ingresarNacimiento(context),
                  SizedBox(width: size.width * 0.13),
                  _genero(context),
                ],
              ),
              //implementacion future builder para seleccionar provincia en base a el pais
              // SizedBox(height: 18),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.spaceAround,
              //   children: <Widget>[
              //     _ingresarEstrato(context),
              //     SizedBox(width: size.width * 0.43),
              //   ],
              // ),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _datosPersonales(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        //physics: const NeverScrollableScrollPhysics(), //*Evita el scroll de una pagina*//
        child: Column(
      children: <Widget>[
        Container(
          width: size.width * 0.8,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            children: <Widget>[
              _ingresarPrimerNombre(context),
              _ingresarSegundoNombre(context),
              _ingresarPrimerApellido(context),
              _ingresarSegundoApellido(context)
              // Row(
              //   children: <Widget>[
              //     _ingresarPrimerNombre(context),
              //     SizedBox(width: size.width * 0.06),
              //     _ingresarSegundoNombre(context)
              //   ],
              // ),
              // SizedBox(height: 18),
              // Row(
              //   children: <Widget>[
              //     _ingresarPrimerApellido(context),
              //     SizedBox(width: size.width * 0.06),
              //     _ingresarSegundoApellido(context)
              //   ],
              // ),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _datosContacto(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        //physics: const NeverScrollableScrollPhysics(), //*Evita el scroll de una pagina*//
        child: Column(
      children: <Widget>[
        Container(
          width: size.width * 0.8,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  _ingresarCorreo(context, bloc),
                  SizedBox(width: size.width * 0.06),
                  _ingresarPassword(context, bloc)
                ],
              ),
              SizedBox(height: 18),
              Row(
                children: <Widget>[
                  _ingresarPais(context, 1),
                  SizedBox(width: size.width * 0.06),
                  _ingresarDepartamento(context, 1)
                ],
              ),
              SizedBox(height: 8),
              Row(
                children: <Widget>[
                  _ingresarCiudad(context, 1),
                  SizedBox(width: size.width * 0.06),
                  _ingresarTelefono(context)
                ],
              ),
              // Row(
              //   children: <Widget>[
              //     _ingresarDireccion(context),
              //     SizedBox(width: size.width * 0.06),
              //     _ingresarCodigoPostal(context),
              //   ],
              // ),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _datosNacimiento(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        //physics: const NeverScrollableScrollPhysics(), //*Evita el scroll de una pagina*//
        child: Column(
      children: <Widget>[
        Container(
          width: size.width * 0.8,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  _ingresarPaisOrigen(context, 2),
                  SizedBox(width: size.width * 0.06),
                  _ingresarDepartamentoOrigen(context, 2)
                ],
              ),
              SizedBox(height: 8),
              Row(
                children: <Widget>[
                  _ingresarCiudadOrigen(context, 2),
                  SizedBox(width: size.width * 0.06),
                  // _ingresarTelefono(context)
                ],
              ),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _datosAcademicos(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
        //physics: const NeverScrollableScrollPhysics(), //*Evita el scroll de una pagina*//
        child: Column(
      children: <Widget>[
        Container(
          width: size.width * 0.8,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          padding: EdgeInsets.symmetric(vertical: 10.0),
          child: Column(
            children: <Widget>[
              Row(
                children: <Widget>[
                  _ingresarGrado(context),
                  SizedBox(width: size.width * 0.06),
                  _ingresarCurso(context)
                ],
              ),
            ],
          ),
        ),
      ],
    ));
  }

  Widget _ingresarPrimerNombre(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Primer nombre',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          _name = value;
        },
      ),
    );
  }

  Widget _ingresarSegundoNombre(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Segundo nombre',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          _surname = value;
        },
      ),
    );
  }

  Widget _ingresarPrimerApellido(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Primer apellido',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          _last_name1 = value;
        },
      ),
    );
  }

  Widget _ingresarSegundoApellido(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Segundo apellido',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          _last_name2 = value;
        },
      ),
    );
  }

  Widget _ingresarNacimiento(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final container = Container(
      width: size.width * 0.3,
      alignment: Alignment.centerLeft,
      child: Text(
        _selectedDate
            ? DateFormat('d MMMM yyyy').format(_birthDate)
            : 'Fecha de nacimiento',
        style: TextStyle(
          fontSize: 12.0,
          // fontWeight: FontWeight.w600,
          height: 0.8,
          color: Colors.black54,
        ),
      ),
      height: size.height * 0.051,
    );
    return GestureDetector(
      child: container,
      onTap: () {
        _birthDay(context);
      },
    );
  }

  // _genero y _tipodocumento se pueden optimizar creando un metodo
  Widget _genero(BuildContext context) {
    final size = MediaQuery.of(context).size;
    List<DropdownMenuItem<String>> generos = new List();

    _genderTypes.forEach((type) {
      generos.add(DropdownMenuItem(
          child: Text(
            type,
            style: TextStyle(
              fontSize: 12.0,
              // fontWeight: FontWeight.w600,
              height: 0.8,
              color: Colors.black54,
            ),
          ),
          value: type));
    });

    return Container(
      width: size.width * 0.37,
      child: DropdownButton(
        elevation: 0,
        value: _gender,
        items: generos,
        style: TextStyle(
          fontSize: 15.0,
          fontWeight: FontWeight.w600,
          height: 0.8,
          color: Colors.black,
        ),
        onChanged: (opt) {
          setState(() {
            _gender = opt;
          });
        },
      ),
    );
  }

  Widget _tipoDocumento(BuildContext context) {
    final size = MediaQuery.of(context).size;
    List<DropdownMenuItem<String>> lista = new List();

    _documentTypes.forEach((type) {
      lista.add(DropdownMenuItem(child: Text(type), value: type));
    });
    return Column(
      children: <Widget>[
        SizedBox(
          height: 15,
        ),
        Container(
          width: size.width * 0.38,
          child: DropdownButton(
            value: _documentType,
            items: lista,
            style: Theme.of(context)
                .textTheme
                .subhead
                .apply(color: Colors.black54, fontSizeFactor: 0.723),
            onChanged: (opt) {
              setState(() {
                _documentType = opt;
              });
            },
          ),
        )
      ],
    );
  }

  Widget _numeroDocumento(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.36,
      child: TextField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: 'Numero de documento',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          _idDocument = value;
        },
      ),
    );
  }

  Widget _ingresarCorreo(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          width: size.width * 0.37,
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
              hintText: 'Correo',
              errorText: snapshot.error,
              errorStyle: TextStyle(
                  color: Colors.white70,
                  fontSize: 10,
                  fontStyle: FontStyle.italic),
              hintStyle: TextStyle(
                fontSize: 12.0,
                // fontWeight: FontWeight.w600,
                height: 0.8,
                color: Colors.black54,
              ),
            ),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  Widget _ingresarTelefono(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.37,
      child: TextField(
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
          hintText: 'Celular',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          setState(() {
            _phone = value;
          });
        },
      ),
    );
  }

  Widget _ingresarCurso(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.37,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Curso',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          setState(() {
            _course = value;
          });
        },
      ),
    );
  }

  Widget _ingresarGrado(BuildContext context) {
    final size = MediaQuery.of(context).size;
    List<DropdownMenuItem<String>> lista = new List();

    _grades.forEach((type) {
      lista.add(DropdownMenuItem(child: Text(type), value: type));
    });
    return Column(
      children: <Widget>[
        SizedBox(height: 15),
        Container(
          width: size.width * 0.37,
          alignment: Alignment.bottomCenter,
          child: DropdownButton(
            value: _grade,
            items: lista,
            style: TextStyle(
              fontSize: 12.0,
              // fontWeight: FontWeight.w600,
              height: 0.8,
              color: Colors.black54,
            ),
            onChanged: (value) {
              setState(() {
                _grade = value;
              });
            },
          ),
        )
      ],
    );
  }

  Widget _ingresarDireccion(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.37,
      child: TextField(
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          hintText: 'Dirección',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          setState(() {
            _direction = value;
          });
        },
      ),
    );
  }

  // dropdownbutton pais
  Widget _ingresarPais(BuildContext context, int caso) {
    final size = MediaQuery.of(context).size;
    return FutureBuilder(
        future: registroProvider.getCountry(),
        builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.contains("Error")) {
              return Container(
                  width: size.width * 0.37,
                  child: Text(
                    "Error en la conexión",
                    style: Theme.of(context)
                        .textTheme
                        .subhead
                        .apply(color: Colors.black54, fontSizeFactor: 0.723),
                  ));
            }

            List<DropdownMenuItem> countrieItems = new List();

            countrieItems.add(DropdownMenuItem(
              child: Container(
                width: size.width * 0.3001,
                child: Text(
                  "Seleccione un pais ",
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .apply(color: Colors.black54, fontSizeFactor: 0.75),
                ),
              ),
              value: "Seleccione un pais",
            ));
            snapshot.data.forEach((countrie) {
              countrieItems.add(DropdownMenuItem(
                child: Text(countrie['name']),
                value: countrie['id'],
              ));
            });

            return Container(
              width: size.width * 0.4,
              child: DropdownButton(
                isExpanded: false,
                value: caso == 1 ? _country : _originCountry,
                items: countrieItems,
                style: TextStyle(
                  fontSize: 13.0,
                  // fontWeight: FontWeight.w600,
                  height: 0.8,
                  color: Colors.black54,
                ),
                onChanged: (opt) {
                  setState(() {
                    caso == 1 ? _country = opt : _originCountry = opt;
                  });
                },
              ),
            );
          } else {
            return Container(
              width: size.width * 0.37,
              child: Text(
                "Espere un momento..",
                style: TextStyle(
                  fontSize: 13.0,
                  // fontWeight: FontWeight.w600,
                  height: 0.8,
                  color: Colors.black54,
                ),
              ),
            );
          }
        });
  }
  // Dropdown button departamentos

  Widget _ingresarDepartamento(BuildContext context, int caso) {
    final size = MediaQuery.of(context).size;
    if (_country == "Seleccione un pais") {
      return Container(
        width: size.width * 0.37,
        child: Text("Primero selecciona un pais",
            style: Theme.of(context)
                .textTheme
                .subhead
                .apply(color: Colors.black54, fontSizeFactor: 0.74)),
      );
    } else {
      return FutureBuilder(
          future: registroProvider.getProvince(_country),
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.contains("Error")) {
                return Container(
                  width: size.width * 0.37,
                  child: Text(
                    "Error en la conexión",
                    style: TextStyle(
                      fontSize: 13.0,
                      // fontWeight: FontWeight.w600,
                      height: 0.8,
                      color: Colors.red,
                    ),
                  ),
                );
              }

              List<DropdownMenuItem> stateItems = new List();

              stateItems.add(DropdownMenuItem(
                child: Container(
                    width: size.width * 0.4,
                    child: Text(
                      "Seleccione un departamento",
                      style: TextStyle(
                        fontSize: 13.0,
                        // fontWeight: FontWeight.w600,
                        height: 0.8,
                        color: Colors.black54,
                      ),
                    )),
                value: "Seleccione un departamento",
              ));
              snapshot.data.forEach((state) {
                stateItems.add(DropdownMenuItem(
                  child: Text(state['name']),
                  value: state['id'],
                ));
              });

              return Container(
                width: size.width * 0.4,
                child: DropdownButton(
                  isExpanded: false,
                  value: caso == 1 ? _province : _originProvince,
                  items: stateItems,
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                  onChanged: (opt) {
                    setState(() {
                      caso == 1 ? _province = opt : _originProvince = opt;
                    });
                  },
                ),
              );
            } else {
              return Container(
                width: size.width * 0.371,
                child: Text(
                  "Espere un momento..",
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                ),
              );
            }
          });
    }
  }

  //Dropdown button ciudad

  Widget _ingresarCiudad(BuildContext context, int caso) {
    final size = MediaQuery.of(context).size;
    if (_province == "Seleccione un departamento") {
      return Container(
        width: size.width * 0.37,
        child: Text(
          "Primero selecciona una ciudad",
          style: TextStyle(
            fontSize: 13.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
      );
    } else {
      return FutureBuilder(
          future: registroProvider.getCity(_province),
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.contains("Error")) {
                return Container(
                  padding: EdgeInsets.only(top: 8),
                  width: size.width * 0.37,
                  child: Text(
                    "Error en la conexión",
                    style: TextStyle(
                      fontSize: 13.0,
                      // fontWeight: FontWeight.w600,
                      height: 0.8,
                      color: Colors.red,
                    ),
                  ),
                );
              }

              List<DropdownMenuItem> stateItems = new List();

              stateItems.add(DropdownMenuItem(
                child: Text("Selecciona una ciudad"),
                value: "Seleccione una ciudad",
              ));
              snapshot.data.forEach((state) {
                stateItems.add(DropdownMenuItem(
                  child: Text(state['name']),
                  value: state['id'],
                ));
              });

              return Container(
                width: size.width * 0.37,
                child: DropdownButton(
                  isExpanded: false,
                  value: caso == 1 ? _city : _originCity,
                  items: stateItems,
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                  onChanged: (opt) {
                    setState(() {
                      caso == 1 ? _city = opt : _originCity = opt;
                    });
                  },
                ),
              );
            } else {
              return Container(
                width: size.width * 0.37,
                child: Text(
                  "Espere un momento..",
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                ),
              );
            }
          });
    }
  }

  Widget _ingresarCodigoPostal(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.37,
      child: TextField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: 'Codigo Postal',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (value) {
          _postal_code = value;
        },
      ),
    );
  }

  Widget _ingresarEstrato(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.37,
      child: TextField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          hintText: 'Estrato',
          hintStyle: TextStyle(
            fontSize: 12.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
        onChanged: (opt) {
          setState(() {
            _estrato = opt;
          });
        },
      ),
    );
  }

  Widget _botones(BuildContext context, LoginBloc bloc) {
    return StreamBuilder(
        stream: bloc.formValidStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Container(
            margin: EdgeInsets.only(bottom: 20),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Container(
                  height: 40,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      gradient: LinearGradient(
                        colors: [
                          Color.fromRGBO(122, 42, 141, 1),
                          Color.fromRGBO(237, 74, 76, 1)
                        ],
                      )),
                  child: RaisedButton(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                      child: Text(
                        'ATRÁS',
                        style: TextStyle(
                            fontWeight: FontWeight.w300,
                            fontSize: 15.0,
                            color: Colors.white),
                      ),
                      elevation: 0.0,
                      textColor: Colors.white,
                      color: Colors.transparent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50.0)),
                      onPressed: () => Navigator.pushNamed(context,
                          'login') /**TODO: Implementación boton REGISTRO */
                      ),
                ),
                Container(
                  width: 50,
                  height: 50,
                  child: Image.asset('assets/IconoLatamColor.png'),
                ),
                Container(
                    height: 40,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(122, 42, 141, 1),
                            Color.fromRGBO(237, 74, 76, 1)
                          ],
                        )),
                    child: RaisedButton(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                        child: Text(
                          'SIGUIENTE',
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              fontSize: 15.0,
                              color: Colors.white),
                        ),
                        elevation: 0.0,
                        textColor: Colors.white,
                        color: Colors.transparent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                        onPressed: snapshot.hasData
                            ? () {
                                _agree == true
                                    ? _register(context, bloc)
                                    : mostrarAlerta(context,
                                        "Por favor acepte los terminos y condiciones");
                              }
                            : null /**TODO: Implementación boton REGISTRO */
                        )),
              ],
            ),
          );
        });
  }

  Widget _ingresarPassword(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          width: size.width * 0.37,
          child: TextField(
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              hintText: 'Contraseña',
              errorText: snapshot.error,
              errorStyle: TextStyle(
                  color: Colors.white70,
                  fontSize: 10,
                  fontStyle: FontStyle.italic),
              hintStyle: TextStyle(
                fontSize: 12.0,
                // fontWeight: FontWeight.w600,
                height: 0.8,
                color: Colors.black54,
              ),
            ),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

//Datos de Nacimiento

  Widget _ingresarPaisOrigen(BuildContext context, int caso) {
    final size = MediaQuery.of(context).size;
    return FutureBuilder(
        future: registroProvider.getCountry(),
        builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.contains("Error")) {
              return Container(
                  width: size.width * 0.37,
                  child: Text(
                    "Error en la conexión",
                    style: Theme.of(context)
                        .textTheme
                        .subhead
                        .apply(color: Colors.black54, fontSizeFactor: 0.723),
                  ));
            }

            List<DropdownMenuItem> countrieItems = new List();

            countrieItems.add(DropdownMenuItem(
              child: Container(
                width: size.width * 0.3001,
                child: Text(
                  "Seleccione un pais ",
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .apply(color: Colors.black54, fontSizeFactor: 0.75),
                ),
              ),
              value: "Seleccione un pais",
            ));
            snapshot.data.forEach((countrie) {
              countrieItems.add(DropdownMenuItem(
                child: Text(countrie['name']),
                value: countrie['id'],
              ));
            });

            return Container(
              width: size.width * 0.4,
              child: DropdownButton(
                isExpanded: false,
                value: caso == 1 ? _country : _originCountry,
                items: countrieItems,
                style: TextStyle(
                  fontSize: 13.0,
                  // fontWeight: FontWeight.w600,
                  height: 0.8,
                  color: Colors.black54,
                ),
                onChanged: (opt) {
                  setState(() {
                    _originCountry = opt;
                  });
                },
              ),
            );
          } else {
            return Container(
              width: size.width * 0.37,
              child: Text(
                "Espere un momento..",
                style: TextStyle(
                  fontSize: 13.0,
                  // fontWeight: FontWeight.w600,
                  height: 0.8,
                  color: Colors.black54,
                ),
              ),
            );
          }
        });
  }
  // Dropdown button departamentos

  Widget _ingresarDepartamentoOrigen(BuildContext context, int caso) {
    final size = MediaQuery.of(context).size;
    if (_originCountry == "Seleccione un pais") {
      return Container(
        width: size.width * 0.37,
        child: Text("Primero selecciona un pais",
            style: Theme.of(context)
                .textTheme
                .subhead
                .apply(color: Colors.black54, fontSizeFactor: 0.74)),
      );
    } else {
      return FutureBuilder(
          future: registroProvider.getProvince(_originCountry),
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.contains("Error")) {
                return Container(
                  width: size.width * 0.37,
                  child: Text(
                    "Error en la conexión",
                    style: TextStyle(
                      fontSize: 13.0,
                      // fontWeight: FontWeight.w600,
                      height: 0.8,
                      color: Colors.red,
                    ),
                  ),
                );
              }

              List<DropdownMenuItem> stateItems = new List();

              stateItems.add(DropdownMenuItem(
                child: Container(
                    width: size.width * 0.4,
                    child: Text(
                      "Seleccione un departamento",
                      style: TextStyle(
                        fontSize: 13.0,
                        // fontWeight: FontWeight.w600,
                        height: 0.8,
                        color: Colors.black54,
                      ),
                    )),
                value: "Seleccione un departamento",
              ));
              snapshot.data.forEach((state) {
                stateItems.add(DropdownMenuItem(
                  child: Text(state['name']),
                  value: state['id'],
                ));
              });

              return Container(
                width: size.width * 0.4,
                child: DropdownButton(
                  isExpanded: false,
                  value: caso == 1 ? _province : _originProvince,
                  items: stateItems,
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                  onChanged: (opt) {
                    setState(() {
                      _originProvince = opt;
                    });
                  },
                ),
              );
            } else {
              return Container(
                width: size.width * 0.371,
                child: Text(
                  "Espere un momento..",
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                ),
              );
            }
          });
    }
  }

  //Dropdown button ciudad

  Widget _ingresarCiudadOrigen(BuildContext context, int caso) {
    final size = MediaQuery.of(context).size;
    if (_originProvince == "Seleccione un departamento") {
      return Container(
        width: size.width * 0.37,
        child: Text(
          "Primero selecciona una ciudad",
          style: TextStyle(
            fontSize: 13.0,
            // fontWeight: FontWeight.w600,
            height: 0.8,
            color: Colors.black54,
          ),
        ),
      );
    } else {
      return FutureBuilder(
          future: registroProvider.getCity(_originProvince),
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data.contains("Error")) {
                return Container(
                  padding: EdgeInsets.only(top: 8),
                  width: size.width * 0.37,
                  child: Text(
                    "Error en la conexión",
                    style: TextStyle(
                      fontSize: 13.0,
                      // fontWeight: FontWeight.w600,
                      height: 0.8,
                      color: Colors.red,
                    ),
                  ),
                );
              }

              List<DropdownMenuItem> stateItems = new List();

              stateItems.add(DropdownMenuItem(
                child: Text("Selecciona una ciudad"),
                value: "Seleccione una ciudad",
              ));
              snapshot.data.forEach((state) {
                stateItems.add(DropdownMenuItem(
                  child: Text(state['name']),
                  value: state['id'],
                ));
              });

              return Container(
                width: size.width * 0.37,
                child: DropdownButton(
                  isExpanded: false,
                  value: caso == 1 ? _city : _originCity,
                  items: stateItems,
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                  onChanged: (opt) {
                    setState(() {
                      _originCity = opt;
                    });
                  },
                ),
              );
            } else {
              return Container(
                width: size.width * 0.37,
                child: Text(
                  "Espere un momento..",
                  style: TextStyle(
                    fontSize: 13.0,
                    // fontWeight: FontWeight.w600,
                    height: 0.8,
                    color: Colors.black54,
                  ),
                ),
              );
            }
          });
    }
  }

  // _launchURL() async {
  // const url = 'https://pruebaslatam.com/Terminoscondiciones';
  // if (await canLaunch(url)) {
  //   await launch(url);
  //   } else {
  //     throw 'Could not launch $url';
  //   }
  // }

}
