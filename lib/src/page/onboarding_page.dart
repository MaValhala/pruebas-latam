import 'dart:async';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/simulacro_provider.dart';
import 'package:xplora/src/utils/utils.dart';

class LoadingPage extends StatefulWidget {

  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/PaginaLogin.png'), fit: BoxFit.cover)),
      child: _content(context),
    ));
  }

  SimulacroProvider _simulacroProvider = new SimulacroProvider();

  Widget _content(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Container(
          width: size.width * 0.95,
          child: _text(context),
        ),

        // _button(context)
      ],
    );
  }

  Widget _logo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.35,
      width: size.width * 0.6,
      // margin: EdgeInsets.symmetric(horizontal: size.width * 0.2, vertical: 10),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                'assets/swipe.gif',
              ),
              fit: BoxFit.contain)),
    );
  }

  Widget _text(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        // textDirection: TextDirection.rtl,
        children: <Widget>[
          Row(
            children: <Widget>[
              SizedBox(width: size.width*0.115,),
              _logo(context),
          ],),
          Text(
            'Para pasar de una pregunta a otra recuerda deslizar tu dedo a la derecha o a la izquierda. También es importante saber que algunas preguntas tienen imágenes, estas puede tardar en mostrarse dependiendo de tu conexión a internet.',
            style:
                Theme.of(context).textTheme.subhead.apply(color: Colors.white),
            textAlign: TextAlign.justify,
          ),
          SizedBox(
            height: size.height * 0.03,
          ),
          Container(
            width: size.width * 0.9,
            child: Text(
              'Sugerencia:',
              style:
                  Theme.of(context).textTheme.title.apply(color: Colors.white),
              textAlign: TextAlign.left,
            ),
          ),
          SizedBox(
            height: size.height * 0.02,
          ),
          Text(
            'Para ganar monedas puedes realizar la revisión de la pregunta.',
            style:
                Theme.of(context).textTheme.subhead.apply(color: Colors.white),
            textAlign: TextAlign.justify,
          ),
          SizedBox(
            height: size.height * 0.06,
          ),
          _button(context)
        ],
      ),
    );
  }

  Widget _button(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);
    return RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Text(
            'INICIAR PRUEBA',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
        ),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        elevation: 0.0,
        color: Colors.white,
        textColor: Colors.purple,
        disabledTextColor: Colors.white10,
        onPressed: () {
          bloc.type == '4' ?_generarSimulacro2(bloc, context)  :_generarSimulacro(bloc, context);
        } /**TODO: Implementación boton INICIAR SESION */
        );
  }

  _generarSimulacro(LoginBloc bloc, BuildContext context) async {
    try {
      loadingsMessage(context,'Generando prueba...');
      Map info = await _simulacroProvider.obtenerSimulacro(
          bloc.pin, bloc.area, bloc.type, context);
      if (info['status']) {
        Timer((Duration(seconds: 5)), (){
          Navigator.pushNamedAndRemoveUntil(context, 'simulacro', (Route<dynamic> route) => false, arguments: _simulacroProvider.simulacro);
        });
        
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    } catch (e) {}
  }


   _generarSimulacro2(LoginBloc bloc, BuildContext context) async {
    try{
      loadingsMessage(context,'Generando prueba...');
      Map info = await _simulacroProvider.obtenerSimulacro2(bloc.pin,bloc.area, bloc.type, context);
      if (info['status']) {
        Navigator.pushNamedAndRemoveUntil(context, 'simulacro', (Route<dynamic> route) => false, arguments: _simulacroProvider.simulacro);
      } else {
        Navigator.pop(context);
        var a = info['message'];
        mostrarAlerta(context,a['message']);
        
      }
    }catch(e){}
  }





}
