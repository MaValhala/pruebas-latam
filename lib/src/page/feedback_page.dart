import 'dart:io';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/providers/login_provider.dart';
// import 'package:xplora/src/utils/utils.dart';

class FeedbackPage extends StatefulWidget {
  const FeedbackPage({Key key}) : super(key: key);

  @override
  _FeedbackPageState createState() => _FeedbackPageState();
}

class _FeedbackPageState extends State<FeedbackPage> {
  Size s;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    s=size;
    Map info = ModalRoute.of(context).settings.arguments;
    final bloc = ProviderLogin.of(context);
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(size.height * 0.15),
            child: SafeArea(
              child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    // boxShadow: [
                    //   BoxShadow(
                    //     blurRadius: 1,
                    //   )
                    // ],
                  ),
                  alignment: Alignment.center,
                  height: size.height * 0.15,
                  child: Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 10),
                    child: Image(
                      image: AssetImage('assets/latam/pruebasLatam.png'),
                      fit: BoxFit.cover,
                    ),
                  )),
            )),
        body: Container(
          width: size.width,
          height: size.height,
          decoration: BoxDecoration(
            color: Colors.white,
            image: DecorationImage(
              image: AssetImage('assets/latam/feedback_background.png'),
              fit: BoxFit.cover,
            ),
          ),
          child: _feedBack(context, bloc, info),
        )));
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.05),
                Text(
                  '¿Quieres omitir la revisión?',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: s.height * 0.02,
                ),
              ],
            ),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  color: Colors.purple,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text('No',
                        style: Theme.of(context).textTheme.subhead.apply(
                              color: Colors.white,
                            )),
                  ),
                  onPressed: () => Navigator.pop(context, false),
                ),
                // SizedBox(width: 15,),
                FlatButton(
                  color: Colors.red,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text(
                      'Si',
                      style: Theme.of(context).textTheme.subhead.apply(
                            color: Colors.white,
                          ),
                    ),
                  ),
                  onPressed: () => Navigator.pushNamedAndRemoveUntil(context, '/', (Route<dynamic> route) => false),
                ),
              ],
            ),
          );
        });
  }

  Widget _feedBack(BuildContext context, LoginBloc bloc, Map info) {
    final size = MediaQuery.of(context).size;
    Color color;
    String mensaje;
    info['points_won'] < 0 ? color = Colors.red : color = Colors.purple;
    info['points_won'] < 0
        ? mensaje =
            'Te recomendamos realizar la revisión del simulacro para mejorar tus habilidades.'
        : mensaje = '¡Felicitaciones!';
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        
        Column(
          children: <Widget>[
            Text(
              'Hola ${bloc.name}, aqui finaliza la prueba', // estudiante se cambiaria por el nombre_estudiante
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .apply(color: Colors.purple),
            ),
            Text(
              "Tu nota es ${info['result']}/100", // se cambiaria por el puntaje respectivo
              style: Theme.of(context).textTheme.title.apply(
                    color: Colors.purple,
                    fontSizeFactor: 1.6,
                  ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Text(
              "Monedas ganadas : ${info['coins_won']}", // se cambiaria por el puntaje respectivo
              style: Theme.of(context).textTheme.title.apply(
                    color: Colors.purple,
                    fontSizeFactor: 1,
                  ),
            ),
            SizedBox(
              height: size.height * 0.01,
            ),
            Text(
              "Puntos obtenidos : ${info['points_won']}", // se cambiaria por el puntaje respectivo
              style: Theme.of(context).textTheme.title.apply(
                    color: color,
                    fontSizeFactor: 1,
                  ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(left:8, right:8 ),
          child: Text(
            mensaje,
            textAlign: TextAlign
                .center, // estudiante se cambiaria por el nombre_estudiante
            style: Theme.of(context)
                .textTheme
                .subhead
                .apply(color: Colors.purple, fontSizeFactor: 1.4),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            _feedbackButton(context, bloc, info),
            _buttonAcept(context),
          ],
        )
      ],
    );
  }

  Widget _buttonAcept(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Container(
            height: size.height * 0.07,
            width: size.width * 0.33,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(122, 42, 141, 1),
                    Color.fromRGBO(237, 74, 76, 1)
                  ],
                )),
            child: FlatButton(
              child: Text(
                'Aceptar',
                style: Theme.of(context).textTheme.subhead.apply(color:Colors.white),
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                Navigator.pushReplacementNamed(context, '/');
              },
            )),
      ],
    );
  }

  Widget _feedbackButton(BuildContext context, LoginBloc bloc, Map info) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.coinsStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
            width: size.width * 0.33,
            height: size.height * 0.07,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(122, 42, 141, 1),
                    Color.fromRGBO(237, 74, 76, 1)
                  ],
                )),
            child: FlatButton(
              child: Text(
                'Revisión',
                style: Theme.of(context).textTheme.subhead.apply(color:Colors.white),
                textAlign: TextAlign.center,
              ),
              onPressed: () {
                print(info['simulacro']);
                Navigator.pushNamed(context, 'revision',
                    arguments: info['answers']);
              },
            ));
      },
    );
  }
}
