import 'dart:async';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
// import 'package:xplora/src/models/simulacro_model.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/profile_provider.dart';
// import 'package:xplora/src/providers/simulacro_provider.dart';
// import 'package:xplora/src/utils/utils.dart';
import 'package:firebase_admob/firebase_admob.dart';
import 'package:xplora/src/utils/utils.dart';

// const String testDevice = 'xx';

class RevisionPage extends StatefulWidget {
  @override
  _RevisionPageState createState() => _RevisionPageState();
}

class _RevisionPageState extends State<RevisionPage> {
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    childDirected: true,

    // testDevices: testDevice != null ? <String>[testDevice] : null,
    // keywords: <String>['Book', 'Game'],
    // contentUrl: 'http://foo.com/bar.html',
    // childDirected: true,
    nonPersonalizedAds: true,
  );

  // // int _coins = 0;
  

  @override
  void initState() {
    super.initState();
    FirebaseAdMob.instance.initialize(appId: 'ca-app-pub-1573039097725042~5711232071');
    RewardedVideoAd.instance.load(
        adUnitId: 'ca-app-pub-1573039097725042/4510516804', targetingInfo: targetingInfo);
    RewardedVideoAd.instance.listener =
        (RewardedVideoAdEvent event, {String rewardType, int rewardAmount}) {
      print("RewardedVideoAd event $event");
      
      if (event == RewardedVideoAdEvent.loaded) {
        // RewardedVideoAd.instance.show();
      }
    };
  }

  @override
  void dispose() {
    super.dispose();
  }

  // SimulacroProvider simulacroProvider = new SimulacroProvider();
  ProfileProvider profileProvider = new ProfileProvider();
  @override
  Widget build(BuildContext context) {
    List simulacro = ModalRoute.of(context).settings.arguments;
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(size.height * 0.15),
          child: SafeArea(
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 3,
                    )
                  ],
                ),
                alignment: Alignment.center,
                height: size.height * 0.15,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Image(
                    image: AssetImage('assets/latam/pruebasLatam.png'),
                    fit: BoxFit.cover,
                  ),
                )),
          )),
      body: Container(
        child: ListView(
          children: _body(context, simulacro),
          physics: BouncingScrollPhysics(),
        ),
        // decoration: BoxDecoration(
        //   // image: DecorationImage(
        //   //   image: AssetImage('assets/PaginaLogin.png',

        //   //   ),
        //   //   fit: BoxFit.cover
        //   // ),
        //   gradient: LinearGradient(colors: [
        //     Color.fromRGBO(122, 42, 141, 0.7),
        //     Color.fromRGBO(237, 74, 76, 0.7),
        //   ]),
        // ),
      ),
      bottomNavigationBar: _bottomBar(context),
    );
  }

  List<Widget> _body(
    BuildContext context,
    List answers,
  ) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    int a = 0;
    return answers.map((item) {
      a++;

      final card = Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.fromLTRB(10, 8, 10, 8),
        child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              // gradient: LinearGradient(
              //   colors: [
              //     Color.fromRGBO(122, 42, 141, 1),
              //     Color.fromRGBO(237, 74, 76, 0.7),
              //   ]
              // ),
              color: item['status'] == true
                  ? Color.fromRGBO(208, 254, 2017, 1)
                  : Color.fromRGBO(254, 208, 208, 1)),
          width: size.width,
          height: size.height * 0.13,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                'Pregunta $a',
                style: Theme.of(context).textTheme.subtitle.apply(
                    color: Colors.black54,
                    fontSizeFactor: size.height * 0.0023),
              ),
              SizedBox(
                width: 1,
              ),
              Container(
                width: size.width * 0.1,
                height: size.height * 0.8,
                child: Image(
                    image: item['status'] == false
                        ? AssetImage('assets/latam/next.png')
                        : AssetImage('assets/latam/nextGreen.png')),
              )
            ],
          ),
        ),
        elevation: 1,
      );
      return GestureDetector(
          child: card,
          onTap: () async {
            try {
              await RewardedVideoAd.instance.load(
                adUnitId: 'ca-app-pub-1573039097725042/4510516804',
                targetingInfo: targetingInfo
              );
              loadings(context);
              Timer(Duration(seconds: 4), () {
                Navigator.pop(context);
                RewardedVideoAd.instance.show().catchError((e) {
                  Navigator.pop(context);
                  Navigator.pushNamed(context, 'revision_details', arguments: item);
                });
                RewardedVideoAd.instance.listener =
                      (RewardedVideoAdEvent event,
                          {String rewardType, int rewardAmount}) {
                    // print("RewardedVideoAd event $event");
                    if (event == RewardedVideoAdEvent.completed) {
                      setState(() {
                        
                        RewardedVideoAd.instance.load(
                            adUnitId: 'ca-app-pub-1573039097725042/4510516804',
                            targetingInfo: targetingInfo);
                      });
                    } 
                    // if (event == RewardedVideoAdEvent.closed) {
                    //   setState(() {
                    //     // print("::debug:: ads should be reloaded");
                    //     // _ad = false;
                    //     RewardedVideoAd.instance.load(
                    //         adUnitId: 'ca-app-pub-1573039097725042/4510516804',
                    //         targetingInfo: targetingInfo);
                    //   });
                    // }
                    if (event == RewardedVideoAdEvent.rewarded) {
                      RewardedVideoAd.instance.load(
                        adUnitId: 'ca-app-pub-1573039097725042/4510516804',
                        targetingInfo: targetingInfo
                      );
                      _updateCoins(bloc, context, item);
                      
                    }
                    
                  };
              });
            } catch (e) {
              mostrarAlerta(context, 'La conexión a internet es muy lenta en este momento, vuelve a intenarlo.');
            }
          });
    }).toList();
  }

  Widget _bottomBar(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.09,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 5,
          )
        ],
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _backButton(context),
          Container(
            height: size.height * 0.05,
            child: Image(image: AssetImage('assets/latam/bottomLogo.png')),
          ),
          _homeButton(context)
        ],
      ),
    );
  }

  Widget _backButton(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FlatButton(
      child: Container(
        height: size.height * 0.05,
        width: size.width * 0.25,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(122, 42, 141, 1),
              Color.fromRGBO(237, 74, 76, 1)
            ],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Icon(
            //   Icons.navigate_before,
            //   color: Colors.white,
            //   size: size.width * 0.052,
            // ),
            Text(
              'REGRESAR',
              style: TextStyle(fontSize: 12, color: Colors.white),
            ),
          ],
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  Widget _homeButton(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FlatButton(
      child: Container(
        height: size.height * 0.05,
        width: size.width * 0.25,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(122, 42, 141, 1),
              Color.fromRGBO(237, 74, 76, 1)
            ],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Icon(
            //   Icons.navigate_before,
            //   color: Colors.white,
            //   size: size.width * 0.052,
            // ),
            Text(
              'SALIR',
              style: TextStyle(fontSize: 12, color: Colors.white),
            ),
          ],
        ),
      ),
      onPressed: () {
        Navigator.pushNamedAndRemoveUntil(
            context, '/', (Route<dynamic> route) => false);
      },
    );
  }

  _updateCoins(LoginBloc bloc, BuildContext context, Map item) async {
    try {
      loadings(context);
      RewardedVideoAd.instance.load(
                adUnitId: 'ca-app-pub-1573039097725042/4510516804',
                targetingInfo: targetingInfo
              );
      Map info = await profileProvider.actualizarCoinsAfterAds(bloc.pin);
      if (info['status']) {
        Navigator.pop(context);
        Navigator.pushNamed(context, 'revision_details', arguments: item);
        mostrarAlerta5(context,'¡Has ganado 20 monedas!');
        bloc.setCoins(info['coins']);
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    } catch (e) {}
  }

}
