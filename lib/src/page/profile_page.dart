import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/page/login_page.dart';
import 'package:xplora/src/providers/login_provider.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {

  var _documentT = {
    '1':'Cedula de Ciudadania',
    '2':'Tarjeta de Identidad',
    '3':'Cedula de Extranjeria',
    '4':'Pasaporte',
    '5':'Registro Civil',
    'Cedula de Ciudadania' : 'Cedula de Ciudadania',
    'Tarjeta de Identidad' : 'Tarjeta de Identidad',
    'Cedula de Extranjeria' : 'Cedula de Extranjeria',
    'Pasaporte' : 'Pasaporte',
    'Registro Civil' :'Registro Civil'

  };
  @override
  Widget build(BuildContext context) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    return Stack(children: <Widget>[
      Image.asset(
        'assets/FondoProfilePage.png',
        height: size.height,
        width: size.width,
        fit: BoxFit.cover,
      ),
      Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Column(
          children: <Widget>[
            _parteSuperior(context, bloc),
            _parteMedia(context, bloc),
            //_parteInferior(context),
          ],
        ),
        ),
      ),
    ]);
  }

  Widget _parteSuperior(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.13,
        width: size.width * 0.9,
        margin: EdgeInsets.only(top: size.height * 0.02),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _points(context, bloc),
            Container(
              margin: EdgeInsets.only(top: size.height * 0.015),
              width: size.width * 0.27,
              child: Image.asset("assets/LatamLogin.png"),
            ),
            _coins(context, bloc),
          ],
        ));
  }

  Widget _points(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.pointStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: size.width * 0.02),
          color: Colors.transparent,
          height: size.height * 0.1,
          width: size.width * 0.22,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.transparent,
                image: DecorationImage(
                    image: AssetImage('assets/ProfilePage1.png'))),
            alignment: Alignment.center,
            width: size.width * 0.27,
            child: Text(
              '    ' + (snapshot.data.toString().length>3?'999':snapshot.data),
              style: Theme.of(context)
                  .textTheme
                  .subtitle
                  .apply(color: Colors.black38, fontSizeFactor: 1.3),
              textAlign: TextAlign.left,
            ),
          ),
        );
      },
    );
  }

  Widget _coins(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.coinsStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
            alignment: Alignment.center,
            color: Colors.transparent,
            height: size.height * 0.1,
            width: size.width * 0.22,
            margin: EdgeInsets.only(right: size.width * 0.001),
            child: Container(
              child: Text(
                '     ' + (snapshot.data.toString().length>3?'999':snapshot.data),
                style: Theme.of(context)
                    .textTheme
                    .subtitle
                    .apply(color: Colors.white, fontSizeFactor: 1.3),
                textAlign: TextAlign.left,
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  image: DecorationImage(
                      image: AssetImage('assets/ProfilePage2.png'))),
              width: size.width * 0.27,
            ));
      },
    );
  }

  Widget _parteMedia(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.855,
        child: Column(
          children: <Widget>[
            _datosPersonales(context, bloc),
            
          ],
        ));
  }

  Widget _datosPersonales(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        _fotoPerfil(context),
        Text('Información personal',
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 20,
                color: Colors.white)),
        Container(
            width: size.width * 0.9,
            height: size.height * 0.06,
            margin: EdgeInsets.only(
                top: size.height * 0.02, left: size.width * 0.055),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _name(context, bloc),
                SizedBox(
                  width: size.width * 0.02,
                ),
                _surnameStream(context, bloc),
              ],
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: Color.fromRGBO(210, 180, 222, 0.1),
            )),
        Container(
            width: size.width * 0.9,
            height: size.height * 0.06,
            margin: EdgeInsets.only(
                top: size.height * 0.02, left: size.width * 0.055),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _lastname1(context, bloc),
                SizedBox(
                  width: size.width * 0.02,
                ),
                _lastname2(context, bloc),
              ],
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: Color.fromRGBO(210, 180, 222, 0.1),
            )),
        _correo(context, bloc),
        _telefono(context, bloc),
        _tipoDocumento(context, bloc),
        _documento(context, bloc),
        SizedBox(height: size.height * 0.03,),
        _botones(context, bloc)
      ],
    );
  }

  Widget _fotoPerfil(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.13,
        width: size.width * 0.5,
        margin: EdgeInsets.only(bottom: size.height * 0.005, top: size.height*0.005),
        child: Image.asset("assets/avatar.png"),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.transparent,
        ));
  }

//bloc.setCoins(info['coins'])
  Widget _name(BuildContext context, LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.nameStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          child: Text(
            snapshot.data,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w700, fontSize: 18, color: Colors.white),
          ),
        );
      },
    );
  }

  Widget _surnameStream(BuildContext context, LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.surnameStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.hasData) {
          return Container(
              child: Text(
            snapshot.data,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w700, fontSize: 18, color: Colors.white),
          ));
        } else {
          return Container(
            child: Text(''),
          );
        }
      },
    );
  }

  Widget _lastname1(BuildContext context, LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.lastname1Stream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          child: Text(
            snapshot.data,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w700, fontSize: 18, color: Colors.white),
          ),
        );
      },
    );
  }

  Widget _lastname2(BuildContext context, LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.lastname2Stream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          child: Text(
            snapshot.data,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w700, fontSize: 18, color: Colors.white),
          ),
        );
      },
    );
  }

  Widget _correo(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.9,
        height: size.height * 0.06,
        margin:
            EdgeInsets.only(top: size.height * 0.02, left: size.width * 0.055),
        child: StreamBuilder(
          stream: bloc.emailStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
              padding: EdgeInsets.only(top: size.height * 0.015),
              child: Text(
                snapshot.data,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.white),
              ),
            );
          },
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Color.fromRGBO(210, 180, 222, 0.1),
        )
    );
  }

  Widget _telefono(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.9,
        height: size.height * 0.06,
        margin:
            EdgeInsets.only(top: size.height * 0.02, left: size.width * 0.055),
        child: StreamBuilder(
          stream: bloc.phoneStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
              padding: EdgeInsets.only(top: size.height * 0.015),
              child: Text(
                snapshot.data,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.white),
              ),
            );
          },
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Color.fromRGBO(210, 180, 222, 0.1),
        ));
  }
  Widget _tipoDocumento(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.9,
        height: size.height * 0.06,
        margin:
            EdgeInsets.only(top: size.height * 0.02, left: size.width * 0.055),
        child: StreamBuilder(
          stream: bloc.documentStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
              padding: EdgeInsets.only(top: size.height * 0.015),
              child: Text(
                _documentT[snapshot.data.toString()],
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.white),
              ),
            );
          },
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Color.fromRGBO(210, 180, 222, 0.1),
        ));
  }
  Widget _documento(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return Container(
        width: size.width * 0.9,
        height: size.height * 0.06,
        margin:
            EdgeInsets.only(top: size.height * 0.02, left: size.width * 0.055),
        child: StreamBuilder(
          stream: bloc.documentTypeStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Container(
              padding: EdgeInsets.only(top: size.height * 0.015),
              child: Text(
                snapshot.data,
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 18,
                    color: Colors.white),
              ),
            );
          },
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: Color.fromRGBO(210, 180, 222, 0.1),
        ));
  }

  Widget _botones(BuildContext context, LoginBloc bloc){
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        _actualizar(context),
        SizedBox(height: size.height * 0.005),
        _cerrarSesion(context, bloc)
      ],
    );
  }

  Widget _actualizar(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: size.width * 0.02),
      child: RaisedButton(
        //padding: EdgeInsets.symmetric(),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.02, vertical: size.height*0.015),
          child: Text(
            'ACTUALIZAR DATOS',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
        ),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        elevation: 0.0,
        color: Colors.white,
        textColor: Colors.purple,
        disabledTextColor: Colors.white10,
        onPressed: () {
          Navigator.pushNamed(context, 'updateProfile');
        }),
    );
  }

  Widget _cerrarSesion(BuildContext context, LoginBloc bloc){
    final size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(left: size.width * 0.02),
      child: RaisedButton(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: size.width * 0.02, vertical: size.height*0.015),
          child: Text(
            'CERRAR SESIÓN',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 16.0,
            ),
          ),
        ),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
        elevation: 0.0,
        color: Colors.white,
        textColor: Colors.purple,
        disabledTextColor: Colors.white10,
        onPressed: () {
          _logout(context, bloc);
        }),
    );
  }

        
  void _logout(BuildContext context, LoginBloc bloc) {
  final size = MediaQuery.of(context).size;
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0),
          ),
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.03),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage('assets/LatamColor.png'),
                ),
              ),
              SizedBox(height: size.height * 0.03),
              Text(
                '¿Deseas cerrar tu sesión?',
                textAlign: TextAlign.center,
              ),
            ],
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              
              // SizedBox(height: size.height*0.03,),
              Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                
                FlatButton(
                  color: Colors.purple,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.16,
                    child: Text('No',
                        style: Theme.of(context).textTheme.subhead.apply(
                              color: Colors.white,
                            )),
                  ),
                  onPressed: () => Navigator.pop(context, false),
                ),
                // SizedBox(width: 15,),
                FlatButton(
                  color: Colors.red,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: size.height * 0.06,
                    width: size.width * 0.16,
                    child: Text(
                      'Si',
                      style: Theme.of(context).textTheme.subhead.apply(
                            color: Colors.white,
                          ),
                    ),
                  ),
                  onPressed: (){
                    bloc.changeEmail('');
                    bloc.changePassword('');
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => LoginPage()), ModalRoute.withName('login'), );
                  },
                ),
              ],
            ),
            ],
          )
          
          
        );
      });
}



}
