import 'package:flutter/material.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:xplora/src/models/simulacro_model.dart';
import 'package:flutter_markdown/flutter_markdown.dart';

class RevisionDetailsPage extends StatelessWidget {
  // const RevisionDetailsPage({Key key}) : super(key: key);
  // final LoginBloc bloc;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final Map pregunta = ModalRoute.of(context).settings.arguments;
    print(pregunta);
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(size.height * 0.15),
          child: SafeArea(
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 3,
                    )
                  ],
                ),
                alignment: Alignment.center,
                height: size.height * 0.15,
                child: Padding(
                  padding: EdgeInsets.only(top: 10, bottom: 10),
                  child: Image(
                    image: AssetImage('assets/latam/pruebasLatam.png'),
                    fit: BoxFit.cover,
                  ),
                )),
          )),
      body: Container(
        width: size.width,
        height: size.height * 0.76,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/FondoApp.png'), fit: BoxFit.cover)),
        child: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(
                  left: size.width * 0.03,
                  top: size.height * 0.05,
                  bottom: size.height * 0.01),
              child: Text(
                'Pregunta: ',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(233, 75, 74, 1)),
              ),
            ),
            Divider(color: Color.fromRGBO(233, 75, 74, 1)),
            Padding(
                padding: EdgeInsets.all(12),
                child: _cards(context, pregunta['content_question'])),
            Container(
              margin: EdgeInsets.only(left: size.width * 0.03,top: size.height * 0.01,),
              child: Text(
                'Respuesta Correcta: ',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(233, 75, 74, 1)),
              ),
            ),
            Divider(color: Color.fromRGBO(233, 75, 74, 1)),
            Container(
              width: size.width * 0.8,
              height: size.height * 0.1,
              margin: EdgeInsets.only(bottom: size.height * 0.02, top: size.height * 0.01),              
              child: Card(
              margin: EdgeInsets.only(left: size.width * 0.05, right: size.width*0.05),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),        
              child: Container(
                padding: EdgeInsets.only(bottom: size.height * 0.01, top: size.height * 0.01, left: size.width * 0.02, right: size.width * 0.02),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color.fromRGBO(208, 254, 2017, 1)),
                width: size.width * 0.8,
                height: size.height * 0.15,
                child: Text(
                      pregunta['content'],
                      textAlign: TextAlign.justify,
                      style: Theme.of(context).textTheme.subtitle.apply(
                          color: Colors.black,
                          fontSizeFactor: size.height * 0.0014),
                    )
              ),
              elevation: 1,
            ),
            ),
            Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                'Explicación: ',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(233, 75, 74, 1)),
              ),
            ),
            Divider(color: Color.fromRGBO(233, 75, 74, 1)),
            Padding(
                padding: EdgeInsets.all(12),
                child: pregunta['explanation'] == 'proximamente'||pregunta['explanation'] == 'Próximamente'||pregunta['explanation'] == 'próximamente'
                    ? Image.network(
                        'http://pruebaslatam.com/img/comming-soon.png')
                    : _cards(context, pregunta['explanation']))
          ],
        ),
      ),
      bottomNavigationBar: _bottomBar(context),
    );
  }

  Widget _cards(BuildContext context, pregunta) {
    String markdown = html2md.convert(pregunta);
    return MarkdownBody(
      data: html2md.convert(markdown).replaceAll('&nbsp;', ' ').replaceAll("**", "".replaceAll('<sub>', '').replaceAll('</sub>', '').replaceAll('<p>', '').replaceAll('</p>', '').replaceAll('<br>', '')).replaceAll('&quot', ''),
    );
  }

  Widget _bottomBar(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.09,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 5,
          )
        ],
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _backButton(context),
          Container(
            height: size.height * 0.05,
            child: Image(image: AssetImage('assets/latam/bottomLogo.png')),
          ),
          _homeButton(context)
        ],
      ),
    );
  }

  Widget _backButton(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FlatButton(
      child: Container(
        height: size.height * 0.05,
        width: size.width * 0.25,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(122, 42, 141, 1),
              Color.fromRGBO(237, 74, 76, 1)
            ],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Icon(
            //   Icons.navigate_before,
            //   color: Colors.white,
            //   size: size.width * 0.052,
            // ),
            Text(
              'REGRESAR',
              style: TextStyle(fontSize: 12, color: Colors.white),
            ),
          ],
        ),
      ),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  Widget _homeButton(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FlatButton(
      child: Container(
        height: size.height * 0.05,
        width: size.width * 0.25,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: LinearGradient(
            colors: [
              Color.fromRGBO(122, 42, 141, 1),
              Color.fromRGBO(237, 74, 76, 1)
            ],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // Icon(
            //   Icons.navigate_before,
            //   color: Colors.white,
            //   size: size.width * 0.052,
            // ),
            Text(
              'SALIR',
              style: TextStyle(fontSize: 12, color: Colors.white),
            ),
          ],
        ),
      ),
      onPressed: () {
        Navigator.pushNamedAndRemoveUntil(
            context, '/', (Route<dynamic> route) => false);
      },
    );
  }
}
