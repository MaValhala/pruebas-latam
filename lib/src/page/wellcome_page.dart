import 'dart:io';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/providers/factions_provider.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/utils/utils.dart';

class WellcomePage extends StatefulWidget {
  // const WellcomePage({Key key}) : super(key: key);
  @override
  _WellcomePageState createState() => _WellcomePageState();
}

class _WellcomePageState extends State<WellcomePage> {
  FactionProvider provider = new FactionProvider();

  Size s;

  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.white,

          // appBar: PreferredSize(
          //     preferredSize: Size.fromHeight(size.height * 0.2),
          //     child: ),
          body: Container(
            width: s.width,
            height: s.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/latam/feedback_background.png'),
                  fit: BoxFit.cover),
            ),
            child: _body(context),
          ),
        ));
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.05),
                Text(
                  '¿Quieres salir de la aplicación?',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: s.height * 0.02,
                ),
              ],
            ),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  color: Colors.purple,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text('No',
                        style: Theme.of(context).textTheme.subhead.apply(
                              color: Colors.white,
                            )),
                  ),
                  onPressed: () => Navigator.pop(context, false),
                ),
                // SizedBox(width: 15,),
                FlatButton(
                  color: Colors.red,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text(
                      'Si',
                      style: Theme.of(context).textTheme.subhead.apply(
                            color: Colors.white,
                          ),
                    ),
                  ),
                  onPressed: () => exit(0),
                ),
              ],
            ),
          );
        });
  }

  Widget _body(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SafeArea(
          child: Container(
              margin: EdgeInsets.all(20),
              alignment: Alignment.center,
              height: size.height * 0.2,
              child: Padding(
                padding: EdgeInsets.only(top: 10, bottom: 5),
                child: Image(
                  image: AssetImage('assets/latam/pruebasLatam.png'),
                  fit: BoxFit.cover,
                ),
              )),
        ),
        _elements(context),
        SizedBox(
          height: size.height * 0.01,
        ),
        // _bottom(context),
      ],
    );
  }

  Widget _elements(BuildContext context) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        SizedBox(
          height: size.height * 0.05,
        ),
        _tittle(context),
        SizedBox(
          height: size.height * 0.1,
        ),
        _message(context),
        SizedBox(
          height: size.height * 0.2,
        ),
        _buton(context, bloc),
      ],
    );
  }

  Widget _tittle(BuildContext context) {
    return Text(
      'Bienvenido a pruebas LATAM',
      style: Theme.of(context).textTheme.title,
    );
  }

  Widget _message(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      child: Text(
        'Aquí podrás elegir el área y el tipo de simulacro que desees realizar, recuerda que una vez que ingreses no podrás salir hasta finalizar cualquiera de las pruebas.',
        style: TextStyle(fontWeight: FontWeight.w400, fontSize: 16),
        textAlign: TextAlign.center,
      ),
    );
  }

  _cargarFaction(LoginBloc bloc, BuildContext context) async {
    provider.cargarData();
    try {
      loadings(context);
      Map info = await provider.obtenerFaction(bloc.pin);
      if (info['status'] && info['faction_id'] != null) {
        final faction = provider.selectedFaction(info['faction_id']);
        bloc.changeFaction(faction);
        bloc.changeName(info['name']);
        bloc.changeSurname(info['sur_name']);
        bloc.changeLastName1(info['last_name1']);
        bloc.changeLastName2(info['last_name2']);
        bloc.changeEmail(info['email']);
        bloc.changePhone(info['cellphone']);
        bloc.changeDocument(info['document']);
        bloc.changeDocumentType(info['documentType']);
        Navigator.pushNamedAndRemoveUntil(
            context, '/', (Route<dynamic> route) => false);
        ;
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    } catch (e) {}
  }

  Widget _buton(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.factionStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          height: size.height * 0.075,
          child: FlatButton(
            onPressed: () {
              _cargarFaction(bloc, context);
            },
            child: Container(
              width: size.width * 0.35,
              height: size.height * 0.07,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                gradient: LinearGradient(colors: [
                  Color.fromRGBO(122, 42, 141, 1),
                  Color.fromRGBO(237, 74, 76, 1)
                ]),
              ),
              child: Text(
                'Entendido',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _bottom(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          height: size.height * 0.085,
          padding: EdgeInsets.only(bottom: 20),
          child: Image(
            image: AssetImage('assets/IconoLatamColor.png'),
            fit: BoxFit.cover,
          ),
        ),
      ],
    );
  }
}
