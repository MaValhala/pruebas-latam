import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/faction_model.dart';
import 'package:xplora/src/page/simulacro_page.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/simulacro_provider.dart';
import 'package:xplora/src/utils/utils.dart';
import 'package:xplora/src/widgets/fancy_bottombar.dart';

class OtherTestPage extends StatelessWidget {
  static const _kFontFam = 'MyFlutterApp';

  static const IconData bell_alt =
      const IconData(0xf0f3, fontFamily: _kFontFam); //Campanita//
  static const IconData rocket =
      const IconData(0xe802, fontFamily: _kFontFam); //Personalizado//
  SimulacroProvider _simulacroProvider = new SimulacroProvider();
  @override
  Widget build(BuildContext context) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    final Faction faction = ModalRoute.of(context).settings.arguments;
    
    return Scaffold(
      body: Container(
        width: size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(faction.background), fit: BoxFit.cover),
        ),
        child: Column(
          children: <Widget>[
            SafeArea(
              child: _appBar(context, faction),
            ),
            _body(context, bloc),
          ],
        ),
      ),
      // bottomNavigationBar: FancyTabBar(
      //   color: faction.color,
      //   back_color: faction.back_color,
      //   faction: faction,
      //   icon_left: bell_alt,
      //   route: '/',
      //   icon_center: rocket,
      // ),
    );
  }

  Widget _appBar(BuildContext context, Faction faction) {
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);
    return Row(
      children: <Widget>[
        Column(
          children: <Widget>[
            _points(context, bloc),
            Container(
              height: size.height * 0.003,
              width: size.width * 0.3825,
              color: Colors.black12,
            ),
            SizedBox(
              height: size.height * 0.049,
            )
          ],
        ),
        Hero(
          tag: faction.name,
          child: Container(
            width: size.width * 0.235,
            child: Image(
              image: AssetImage('assets/minib.png'),
              fit: BoxFit.contain,
            ),
          ),
        ),
        Column(
          children: <Widget>[
            _coins(context, bloc),
            Container(
              height: size.height * 0.003,
              width: size.width * 0.3825,
              color: Colors.black12,
            ),
            SizedBox(
              height: size.height * 0.049,
            )
          ],
        ),
      ],
    );
  }
  Widget _points(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.pointStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: 15),
          color: Colors.white,
          height: size.height * 0.1,
          width: size.width * 0.22,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(image: AssetImage('assets/points.png'))),
            alignment: Alignment.center,
            width: size.width * 0.27,
            child: Text(
              '   ' + (snapshot.data.toString().length>3?'999':snapshot.data),
              style: Theme.of(context)
                  .textTheme
                  .subtitle
                  .apply(color: Colors.black38, fontSizeFactor: 1.1),
              textAlign: TextAlign.left,
            ),
          ),
        );
      },
    );
  }

  Widget _coins(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.coinsStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
            alignment: Alignment.center,
            color: Colors.white,
            height: size.height * 0.1,
            width: size.width * 0.22,
            margin: EdgeInsets.only(right: 15),
            child: Container(
              child: Text(
                '    ' + (snapshot.data.toString().length>3?'999':snapshot.data),
                style: Theme.of(context)
                    .textTheme
                    .subtitle
                    .apply(color: Colors.white, fontSizeFactor: 1.1),
                textAlign: TextAlign.right,
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  image:
                      DecorationImage(image: AssetImage('assets/coins.png'))),
              width: size.width * 0.27,
            ));
      },
    );
  }

  Widget _body(BuildContext context, LoginBloc bloc) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    return Column(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          height: size.height * 0.03,
        ),
        
        Container(
          height: size.height * 0.18,
          child: Image(
            image: AssetImage('assets/LatamColor.png'),
          ),
        ),
        SizedBox(
          height: size.height * 0.08,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25),
          child: Text(
            'Recuerda que el tipo de simulacro PRUEBA PROGRAMADA estará disponible solo en fechas predeterminadas',
            style: Theme.of(context).textTheme.subhead.apply(color: Colors.black54, fontSizeFactor: 1.05),
            textAlign: TextAlign.justify,
          ),
        ),
        SizedBox(
          height: size.height * 0.08,
        ),
        _preIcfes(context, bloc),
        SizedBox(
          height: size.height * 0.055,
        ),
        _pruebaProg(context, bloc)
      ],
    );
  }
  Widget _preIcfes(BuildContext context, LoginBloc bloc){
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.typeStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Color.fromRGBO(122, 43, 141, 1), width: 3),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: Container(
            alignment: Alignment.center,
            height: size.height * 0.09,
            width: size.width * 0.52,
            child: Text(
              'SIMULACRO                 PRE-ICFES',
              style: Theme.of(context)
                  .textTheme
                  .title
                  .apply(color: Color.fromRGBO(122, 43, 141, 1)
              ),
              textAlign: TextAlign.center,
            ),
            
          ),
          onPressed: () {
            bloc.changeType('3');
            Navigator.pushNamed(context, 'onboarding');
          },
        );
      },
    );
  }

  Widget _pruebaProg(BuildContext context, LoginBloc bloc){
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.typeStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return RaisedButton(
          color: Color.fromRGBO(237, 74, 74, 1),
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Color.fromRGBO(237, 74, 74, 1), width: 3),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: Container(
            alignment: Alignment.center,
            height: size.height * 0.09,
            width: size.width * 0.52,
            child: Text(
              'PRUEBA PROGRAMADA',
              style: Theme.of(context)
                  .textTheme
                  .title
                  .apply(color: Colors.white
              ),
              textAlign: TextAlign.center,
            ),
          ),
          onPressed: () {
            bloc.changeType("4");
            Navigator.pushNamed(context, 'onboarding');
          },
        );
      },
    );
  }
 
}
