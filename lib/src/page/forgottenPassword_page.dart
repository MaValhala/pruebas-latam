import 'package:flutter/material.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/utils/utils.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({Key key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {

  String _correo;
  final usuarioProvider = new UsuarioProvider();
  GlobalKey<FormState> _key = GlobalKey();
  RegExp emailRegExp =
      new RegExp(r'^\w+[\w-\.]*\@\w+((-\w+)|(\w*))\.[a-z]{2,3}$');
  // final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          crearFondo(context),
          _contenido(),
          

        ],
      ),
    );
  }
  Widget _contenido(){
    return ListView(
      children: <Widget>[
        logo(context),
        _ingresarEmail(context),
        
        // _botonLogin(context)
      ],
    );
  }
  Widget _regresar(BuildContext context){
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height *0.06,
      width: size.width*0.3,
      // color: Color.fromRGBO(237, 74, 74, 1),
      decoration: BoxDecoration(
        border: Border.all(color:Color.fromRGBO(237, 74, 74, 1),width: 3),
        borderRadius: BorderRadius.circular(50)
      ),
      child: FlatButton(
        onPressed: (){
          Navigator.pop(context);
        }, 
        child: Text(
          'REGRESAR',
          style: TextStyle(
            color: Color.fromRGBO(237, 74, 74, 1)
          ),
        ))
    );
  }
  Widget crearFondo(BuildContext context) {
    final fondoImg = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/FondoApp.png'), fit: BoxFit.cover)),
    );

    return Stack(
      children: <Widget>[
        fondoImg,
      ],
    );
  }

  Widget logo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.4,
      width: size.width * 0.6,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.2, vertical: 20),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/LatamColor.png'),
      )),
    );
  }
  _forgPass(BuildContext context, String email) async {  
    try{
      loadings(context);
      Map info = await usuarioProvider.recuperarPass(email);
      if (info['status']) {   
        Navigator.pop(context);
        // mostrarAlerta4(context, 'Se ha enviado un mensaje con una nueva contraseña a tu correo electrónico.', '');
        Navigator.popAndPushNamed(context, 'recovery_page');
        
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    } catch(e){
    } 
  }
  Widget _ingresarEmail(BuildContext context) {
    final size = MediaQuery.of(context).size;
    
        return Container(
          width: size.width * 0.9,
          height: size.height * 0.5,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: Form(
            key: _key,

            child: Column(
              children: <Widget>[
              //   SizedBox(
              //     height: size.height*0.02,
              //   ),
                TextFormField(
                  
                  autovalidate: true,

                  validator: (value){

                    if (value.length == 0) {
                      return "Ingrese un correo electrónico";
                    } else if(!emailRegExp.hasMatch(value)){
                      return 'Ingrese una dirección de correo valida';
                    }else{
                      return null;
                    }
                  },
                  keyboardType: TextInputType.emailAddress,  
                  cursorColor: Colors.black54,
                  style: TextStyle(color: Colors.black54),
                  decoration: InputDecoration(
                    hintText: 'CORREO ELECTRÓNICO',
                    // counterText: snapshot.data,
                    counterStyle: TextStyle(color: Colors.black54, fontSize: 10, fontStyle: FontStyle.italic),
                    hintStyle: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.w600,
                        height: 0.8,
                        color: Colors.black54,
                    )
                  ),
                  onSaved: (value){
                    setState(() {
                      _correo=value;
                    });
                  },
                ),
                SizedBox(
                  height: size.height*0.07,
                ),
                Container(
                  height: size.height *0.06,
                  child: RaisedButton(
                    //padding: EdgeInsets.symmetric(),
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: size.width * 0.01, vertical: size.height * 0.01),
                      child: Text(
                        'RECORDAR CONTRASEÑA',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0,
                        ),
                      ),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0)),
                    elevation: 0.0,
                    color: Color.fromRGBO(122, 43, 141, 1),
                    textColor: Colors.white,
                    disabledTextColor: Colors.white10,
                    onPressed: () async {
                      if(_key.currentState.validate()){
                        _key.currentState.save();
                        // Navigator.popAndPushNamed(context, 'recovery_page');
                        _forgPass(context, _correo);
                      }
                    }
                    ),
                ),
                SizedBox(
                  height: size.height*0.16,
                ),
                _regresar(context)
              ],
            ),
          )
        );
      
    
  }


}