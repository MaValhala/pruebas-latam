import 'dart:async';
// import 'dart:io';
// import 'dart:js';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/simulacro_model.dart';
import 'package:xplora/src/providers/factions_provider.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/profile_provider.dart';
import 'package:xplora/src/providers/simulacro_provider.dart';
import 'package:xplora/src/utils/utils.dart';
import 'package:xplora/src/widgets/countdown.dart';
// import 'package:xplora/src/widgets/button_widgets.dart';
import 'package:xplora/src/widgets/pregunta_widget.dart';
// import 'package:xplora/src/widgets/questionSwiper_widget.dart';
// import 'package:flutter_html_widget/flutter_html_widget.dart';
class SimulacroPage extends StatefulWidget {
  @override
  _SimulacroPageState createState() => _SimulacroPageState();
 
}

class _SimulacroPageState extends State<SimulacroPage> {

  Timer _timer;
  int _start;
  bool alerta=true;

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  var areas = {
    "1": "CIENCIAS NATURALES",
    "2": "CIENCIAS SOCIALES",
    "3": "LECTURA CRITÍCA",
    "4": "MATEMATICAS",
    "5": "INGLES"
  };
  var colors = {
    "CIENCIAS NATURALES" : Color.fromRGBO(242, 27, 87, 1),
    "CIENCIAS SOCIALES"  : Color.fromRGBO(122, 43, 134, 1),
    "LECTURA CRITÍCA"    : Color.fromRGBO(250, 188, 17, 1),
    "MATEMATICAS"        : Color.fromRGBO(21, 171, 159, 1),
    "INGLES"             : Color.fromRGBO(43, 120, 226, 1),
  };
  
  BuildContext cont;
  SimulacroProvider simulacroProvider = new SimulacroProvider();
  SimulacroModel simulacro;
  FactionProvider factions;
  ProfileProvider profileProvider = new ProfileProvider();
  Size s;
  LoginBloc bloc;
  bool setTime= true;
  bool timeOver = true;
  // bool cargando;
  @override
  void initState() { 
    super.initState();
    // cargando= false;
  }
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    s = size;
    cont=context;
    simulacro = ModalRoute.of(context).settings.arguments;
    bloc      = ProviderLogin.of(context);
    if(setTime){
      bloc.type=='1'?_start=1500
                    :bloc.type=='2'?_start=300
                                   :_start=7500;
      setTime=false;
    }

//mostrarAlerta3(context, 'Ops, se te ha agotado el tiempo, para pruebas latam no es un problema, pero para las pruebas icfes ya te recogieron, maneja mejor tu tiempo.', 'El tiempo ha finalizado!');

    Future.delayed(new Duration(seconds: _start),(){
      if(timeOver){
        timeOver = false;
        return bloc.type=='2'?_contraRelojValidation(simulacro, bloc, context)
                           :mostrarAlerta3(context, 'Puedes continuar resolviendo el simulacro pero tu puntuación sera menor', 'El tiempo ha finalizado!');
      
      }
      
            //  bloc.type=='2'?
            //                :             
    });
    // startTimer();
    
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          appBar: _appBar(context, bloc),
          body: ListView(
            children: <Widget>[
              _questionSwiper(simulacro)
            ],
          ),
          bottomNavigationBar: _bottomBar(context, bloc, simulacro),
        ));
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: cont,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.05),
                Text(
                  '¿Quieres salir del simulacro?',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: s.height * 0.02,
                ),
              ],
            ),
            content: Text(
              'Si abandonas la prueba ahora perderás las monedas que gastaste, estás de acuerdo?',
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.purple,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(12.0)),
                child: Container(
                  alignment: Alignment.center,
                  height: s.height * 0.06,
                  width: s.width * 0.16,
                  child: Text('No',
                      style: Theme.of(context).textTheme.subhead.apply(
                            color: Colors.white,
                          )),
                ),
                onPressed: () => Navigator.pop(context, false),
              ),
              SizedBox(
                width: s.width * 0.05,
              ),
              FlatButton(
                color: Colors.red,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(12.0)),
                child: Container(
                  alignment: Alignment.center,
                  height: s.height * 0.06,
                  width: s.width * 0.16,
                  child: Text(
                    'Si',
                    style: Theme.of(context).textTheme.subhead.apply(
                          color: Colors.white,
                        ),
                  ),
                ),
                onPressed: () {
                  alerta=false;
                  _updateCoins(bloc, context);
                }
              ),
              SizedBox(
                width: s.width * 0.10,
              )
            ],
          );
        });
  }
  _updateCoins(LoginBloc bloc, BuildContext context) async {  
      try{
        loadings(context);
        Map info = await profileProvider.actualizarCoins(bloc.pin);
        if (info['status']) {   
          Navigator.pushNamedAndRemoveUntil(context,'/', (Route<dynamic> route) => false);;
          bloc.setCoins(info['coins']);
        } else {
          Navigator.pop(context);
          mostrarAlerta(context, info['mensaje']);
        }
      } catch(e){
      } 
  }

  Widget _appBar(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return PreferredSize(
        preferredSize: Size.fromHeight(size.height * 0.102),
        child: StreamBuilder(
          stream: bloc.factionStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return SafeArea(
                child: Column(
              children: <Widget>[
                Container(
                  height: size.height * 0.1,
                  alignment: Alignment.center,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      // Container(
                      //   height: size.height * 0.06,
                      //   child: Image(
                      //     image: AssetImage(
                      //       bloc.faction.icon,
                      //     ),
                      //     fit: BoxFit.cover,
                      //   ),
                      // ),
                      SizedBox(
                        width: 5,
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                              bloc.type =="1"?"SIMULACRO"
                              :bloc.type == "2"
                                  ? "CONTRARELOJ"
                                  : bloc.type == "3"
                                  ? 'PRE-ICFES'
                                  : 'PRUEBA PROGRAMADA',
                              style: TextStyle(
                                  color: Color(bloc.faction.color),
                                  fontSize: size.width * 0.06,
                                  fontWeight: FontWeight.w600)
                          ),
                          bloc.area=="6"?Divider(height: size.height*0.0001,)
                                         :Text(
                                            areas[bloc.area],
                                            style: TextStyle(color: colors[areas[bloc.area]]),
                                         ),
                          
                        ],
                      )
                    ],
                  ),
                ),
                Divider(
                  thickness: 2,
                  height: 1,
                )
              ],
            ));
          },
        ));
  }

  Widget _bottomBar(BuildContext context, LoginBloc bloc, SimulacroModel simulacro) {
    var clock = AdvCountdown(futureDate: simulacro.simulacrumMaxTime, type: bloc.type,);
    final size = MediaQuery.of(context).size;
    print(simulacro.simulacrumMaxTime.toString());
    return Container(
      height: size.height * 0.09,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            blurRadius: 3,
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          
            
          clock,
          
          nextButtom(context, 'feedback', bloc, simulacro)
        ],
      ),
    );
  }

  Widget _questionSwiper(SimulacroModel simulacro) {
    return simulacrosPageView(simulacro: simulacro, siguientePagina: null);
  }

  Widget nextButtom(BuildContext context, String route, LoginBloc bloc,
      SimulacroModel simulacro) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.pinStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: Container(
            height: size.height * 0.05,
            width: size.width * 0.25,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(122, 42, 141, 1),
                    Color.fromRGBO(237, 74, 76, 1)
                  ],
                )),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'FINALIZAR',
                  style: TextStyle(fontSize: 12, color: Colors.white),
                ),
                // Icon(
                //   Icons.navigate_next,
                //   color: Colors.white,
                //   size: size.width * 0.052,
                // ),
              ],
            ),
          ),
          onPressed: () {
            bloc.type=='2'?_contraRelojValidation(simulacro, bloc, context)
                          : _otherValidation(simulacro, bloc, context);
          },
        );
      },
    );
  }

  _contraRelojValidation(SimulacroModel simulacro, LoginBloc bloc, BuildContext context) async {
    
     
        
      Future.delayed(new Duration(seconds: 1),(){
        // loadings(context);    
        return _sendAnswers(simulacro, bloc, context);   
      });
        
      //   for (var i = 0; i < simulacro.questions.length; i++) {
      //   if (simulacro.questions[i].selectedAnswer == null) {
      //     return simulacroIncompleto(context, (i + 1).toString());
      //   }
      // }
      
    
  }

  bool complete = false;

  _otherValidation(SimulacroModel simulacro, LoginBloc bloc, BuildContext context) async { 
  try{
    loadings(context);    
    for (var i = 0; i < simulacro.questions.length; i++) {
    if (simulacro.questions[i].selectedAnswer == null) {
      Navigator.pop(context);
      return simulacroIncompleto(context, (i + 1).toString());
    } else {
      if(i == simulacro.questions.length-1){
        Navigator.pop(context);
        _sendAnswers(simulacro, bloc, context);
      }
    }
    }
    
  }catch(e){}   
 }
  
  _sendAnswers(SimulacroModel simulacro, LoginBloc bloc,BuildContext context)async{
    try{
      loadings(context);
      Map info = await simulacroProvider.validarSimulacro(
        bloc.pin, simulacro.simulacrumId, simulacro.toJson(), bloc);

      if (info['status']) {
        info['simulacro'] = simulacro;
        setState(() {
          alerta=false;
        });
        print('ff');
        Navigator.pop(context);
        Navigator.pushNamedAndRemoveUntil(context, 'feedback', ModalRoute.withName('/'), arguments: info);
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    }catch(e){

    }
  }

}
