import 'dart:io';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/providers/login_provider.dart';
class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

Size s;

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // final double width=MediaQuery.of(context).size.width*0.11;
    // final
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);
    s = size;
    return WillPopScope(
        onWillPop: _onBackPressed,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: _appBar(size, bloc),
          body: _menuOpts(context),
          // floatingActionButton: _boton(),
          // floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      )
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.05),
                Text(
                  '¿Quieres salir de la aplicación?',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: s.height * 0.02,
                ),
              ],
            ),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  color: Colors.purple,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text('No',
                        style: Theme.of(context).textTheme.subhead.apply(
                              color: Colors.white,
                            )),
                  ),
                  onPressed: () => Navigator.pop(context, false),
                ),
                // SizedBox(width: 15,),
                FlatButton(
                  color: Colors.red,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text(
                      'Si',
                      style: Theme.of(context).textTheme.subhead.apply(
                            color: Colors.white,
                          ),
                    ),
                  ),
                  onPressed: () => exit(0),
                ),
              ],
            ),
          );
        });
  }

  Widget _appBar(Size size, LoginBloc bloc) {
    return PreferredSize(
        preferredSize: Size.fromHeight(size.height * 0.125),
        child: SafeArea(
            child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                blurRadius: 1,
              )
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              
              // _points(context, bloc),
              // SizedBox(
              //   height: size.height * 0.1,
              // ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _points(context, bloc),
                  _coins(context, bloc),
                ],
              ),
              
              Container(
                  alignment: Alignment.center,
                  // height: size.height * 0.13,
                  width: size.width * 0.27,
                  child: Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 5),
                    child: Image(
                      image: AssetImage('assets/latam/pruebasLatam.png'),
                      //fit: BoxFit.cover,
                    ),
                  )),
                  
              _boton(context),
              
              // _coins(context, bloc),
              
            ],
          ),
        )));
  }

  Widget _points(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.pointStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          alignment: Alignment.center,
          // margin: EdgeInsets.only(left: 15),
          color: Colors.white,
          height: size.height * 0.05,
          width: size.width * 0.26,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(image: AssetImage('assets/points.png'))),
            alignment: Alignment.center,
            width: size.width * 0.27,
            child: Text(
              '   ' + (snapshot.data.toString().length>3?'999':snapshot.data),
              style: Theme.of(context)
                  .textTheme
                  .subtitle
                  .apply(color: Colors.black38, fontSizeFactor: 1.1),
              textAlign: TextAlign.left,
            ),
          ),
        );
      },
    );
  }

  Widget _coins(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    // String d = ;
    return StreamBuilder(
      stream: bloc.coinsStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
            alignment: Alignment.center,
            color: Colors.white,
            height: size.height * 0.05,
            width: size.width * 0.26,
            // margin: EdgeInsets.only(right: 15),
            child: Container(
              child: Text(
                '    ' + (snapshot.data.toString().length>3?'999':snapshot.data),
                style: Theme.of(context)
                    .textTheme
                    .subtitle
                    .apply(color: Colors.white, fontSizeFactor: 1.1),
                textAlign: TextAlign.right,
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  image:
                      DecorationImage(image: AssetImage('assets/coins.png'))),
              width: size.width * 0.27,
            ));
      },
    );
  }

  Widget _menuOpts(BuildContext context) {
    final bloc = ProviderLogin.of(context);

    return ListView(
      children: <Widget>[
        SizedBox(
          height: MediaQuery.of(
                context,
              ).size.width *
              0.025
        ),
        _cienciasNaturales(context, bloc),
        SizedBox(
          height: MediaQuery.of(context).size.width * 0.022,
        ),
        _cienciasSocialesyCiudadanas(context, bloc),
        SizedBox(
          height: MediaQuery.of(context).size.width * 0.022,
        ),
        _lecturaCritica(context, bloc),
        SizedBox(
          height: MediaQuery.of(context).size.width * 0.022,
        ),
        _matematicas(context, bloc),
        SizedBox(
          height: MediaQuery.of(context).size.width * 0.022,
        ),
        _ingles(context, bloc),
        SizedBox(
          height: MediaQuery.of(context).size.width * 0.022,
        ),
        _preIcfes(context, bloc),
        // _boton(),
      ],
    );
  }

  Widget _boton(BuildContext context){
    final size = MediaQuery.of(context).size;
    return FlatButton(
      child: Container(
        width: size.width*0.17,
        child: Image.asset('assets/avatar.png'),
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(50)
      ),
      onPressed: (){
        Navigator.pushNamed(context, 'profile');
      },
      );
 
  }

  Widget _cienciasNaturales(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.areaStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: naturales(size.width * 0.9, size.height * 0.12, 10, context),
          onPressed: () {
            bloc.changeArea("1");
            Navigator.pushNamed(context, 'faction_page',
                arguments: bloc.faction);
          },
        );
      },
    );
  }

  Widget _cienciasSocialesyCiudadanas(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.areaStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: sociales(size.width * 0.9, size.height * 0.12, 10, context),
          onPressed: () {
            bloc.changeArea("2");
            Navigator.pushNamed(context, 'faction_page',
                arguments: bloc.faction);
          },
        );
      },
    );
  }

  Widget _lecturaCritica(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.areaStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: lectura(size.width * 0.9, size.height * 0.12, 10, context),
          onPressed: () {
            bloc.changeArea("3");
            Navigator.pushNamed(context, 'faction_page',
                arguments: bloc.faction);
          },
        );
      },
    );
  }

  Widget _matematicas(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.areaStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: matematicas_container(
              size.width * 0.9, size.height * 0.12, 10, context),
          onPressed: () {
            bloc.changeArea("4");
            print(bloc.area);
            Navigator.pushNamed(context, 'faction_page',
                arguments: bloc.faction);
          },
        );
      },
    );
  }

  Widget _ingles(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.areaStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: ingles_container(
              size.width * 0.9, size.height * 0.12, 10, context),
          onPressed: () {
            bloc.changeArea("5");
            Navigator.pushNamed(context, 'faction_page',
                arguments: bloc.faction);
          },
        );
      },
    );
  }

  Widget _preIcfes(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.areaStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return FlatButton(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(122, 42, 141, 1),
                    Color.fromRGBO(242, 27, 87, 1)
                  ],
                )),
            width: MediaQuery.of(context).size.width * 0.9,
            height: MediaQuery.of(context).size.height * 0.12,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'SIMULACRO',
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .title
                      .apply(color: Colors.white),
                ),
                Text(
                  'PRE-ICFES',
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .title
                      .apply(color: Colors.white),
                ),
              ],
            ),
          ),
          onPressed: () {
            bloc.changeArea("6");
            Navigator.pushNamed(context, 'other_test',
                arguments: bloc.faction);
          },
        );
      },
    );
  }

  Widget naturales(
      double width, double height, double radious, BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        // color: Color.fromRGBO(235, 75, 75, 1),
        gradient: LinearGradient(colors: [Color.fromRGBO(242, 27, 87, 1),Color.fromRGBO(235, 75, 77, 1)] ),
        borderRadius: BorderRadius.circular(radious),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // SizedBox(width:10 ,),
          Text(
            '  Ciencias Naturales',
            style: Theme.of(context)
                .textTheme
                .title
                .apply(color: Colors.white, fontSizeFactor: 0.9),
          ),
          // SizedBox(width:10 ,),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.1,
                child: Image(
                  image: AssetImage('assets/latam/naturales.png'),
                  fit: BoxFit.cover,
                )),
          ),
        ],
      ),
    );
  }

  Widget sociales(
      double width, double height, double radious, BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [Color.fromRGBO(122, 43, 134, 1),Color.fromRGBO(183, 68, 145, 1),] ),
        borderRadius: BorderRadius.circular(radious),
      ),
      width: width,
      height: height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // SizedBox(width:10 ,),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                '  Ciencias Sociales',
                style: Theme.of(context)
                    .textTheme
                    .title
                    .apply(color: Colors.white, fontSizeFactor: 0.9),
              ),
              Text(
                'y Ciudadanas',
                style: Theme.of(context)
                    .textTheme
                    .title
                    .apply(color: Colors.white, fontSizeFactor: 0.9),
              )
            ],
          ),
          // SizedBox(width:10 ,),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.1,
                child: Image(
                  image: AssetImage('assets/latam/humanas.png'),
                  fit: BoxFit.cover,
                )),
          ),
        ],
      ),
    );
  }

  Widget lectura(
      double width, double height, double radious, BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [Color.fromRGBO(250, 188, 17, 1),Color.fromRGBO(252, 234,36, 1)] ),
        borderRadius: BorderRadius.circular(radious),
      ),
      width: width,
      height: height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // SizedBox(width:10 ,),
          Text(
            '   Lectura Crítica',
            style: Theme.of(context)
                .textTheme
                .title
                .apply(color: Colors.white, fontSizeFactor: 0.9),
          ),
          // SizedBox(width:10 ,),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.1,
                child: Image(
                  image: AssetImage('assets/latam/lectura.png'),
                  fit: BoxFit.cover,
                )),
          ),
        ],
      ),
    );
  }

  Widget ingles_container(
      double width, double height, double radious, BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [Color.fromRGBO(43, 120, 226, 1),Color.fromRGBO(44, 167, 227, 1)] ),
        borderRadius: BorderRadius.circular(radious),
      ),
      width: width,
      height: height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // SizedBox(width:10 ,),
          Text(
            '   Inglés',
            style: Theme.of(context)
                .textTheme
                .title
                .apply(color: Colors.white, fontSizeFactor: 0.9),
          ),
          // SizedBox(width:MediaQuery.of(context).size.width*0.55  ,),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 0, 20, 0),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.1,
                child: Image(
                  image: AssetImage('assets/latam/ing.png'),
                  fit: BoxFit.cover,
                )),
          ),
        ],
      ),
    );
  }

  Widget matematicas_container(
      double width, double height, double radious, BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [Color.fromRGBO(21, 171, 159, 1),Color.fromRGBO(96, 219, 154, 1)] ),
        borderRadius: BorderRadius.circular(radious),
      ),
      width: width,
      height: height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // SizedBox(width:10 ,),
          Text(
            '   Matemáticas',
            style: Theme.of(context)
                .textTheme
                .title
                .apply(color: Colors.white, fontSizeFactor: 0.9),
          ),
          // SizedBox(width:MediaQuery.of(context).size.width*0.43  ,),
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Container(
                // padding: EdgeInsetsGeometry,
                width: MediaQuery.of(context).size.width * 0.1,
                child: Image(
                  image: AssetImage('assets/latam/mate.png'),
                  fit: BoxFit.cover,
                )),
          ),
        ],
      ),
    );
  }
}
