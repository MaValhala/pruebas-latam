import 'dart:io';

import 'package:flutter/material.dart';

import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/utils/utils.dart';
import 'package:package_info/package_info.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final usuarioProvider = new UsuarioProvider();
 
  String _version;
  String _coins;
  bool alertVer = false;
  String _points;

  String _pin;
  Size s;

  @override
  Widget build(BuildContext context) {
    s= MediaQuery.of(context).size;
    _ver();
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            crearFondo(context),
            _contenido(context),
            
          ],
        ),
      )
    );
  }
  _ver() async{
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    String version = packageInfo.version;
    Map info = await usuarioProvider.version();
      if (info['status']) {   
          
      } else {
        
    }
    if(version==info['version']){
      
    }else{
      if(!alertVer){
        versionAlert(context);
        setState(() {
          alertVer= true;
        });
      }
      
    }
  }
  Future<bool> _onBackPressed() {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.05),
                Text(
                  '¿Quieres salir de la aplicación?',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: s.height * 0.02,
                ),
              ],
            ),
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                FlatButton(
                  color: Colors.purple,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text('No',
                        style: Theme.of(context).textTheme.subhead.apply(
                              color: Colors.white,
                            )),
                  ),
                  onPressed: () => Navigator.pop(context, false),
                ),
                // SizedBox(width: 15,),
                FlatButton(
                  color: Colors.red,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(12.0)),
                  child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text(
                      'Si',
                      style: Theme.of(context).textTheme.subhead.apply(
                            color: Colors.white,
                          ),
                    ),
                  ),
                  onPressed: () => exit(0),
                ),
              ],
            ),
          );
        });
  }
  Widget crearFondo(BuildContext context) {
    final fondoImg = Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/PaginaLogin.png'), fit: BoxFit.cover)),
    );

    return Stack(
      children: <Widget>[
        fondoImg,
      ],
    );
  }

  Widget _contenido(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        //physics: const NeverScrollableScrollPhysics(), //*Evita el scroll de una pagina*//
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            logo(context),
            _inputs(context),
            // _forgotenPassword(),
            _registro(context),
            _iconos(context),
          ],
        ),
      ),
    );
  }

  Widget logo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.35,
      width: size.width * 0.6,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.2, vertical: 10),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/LatamLogin.png'),
      )),
    );
  }

  Widget _inputs(BuildContext context) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.35,
      width: size.width * 0.9,
      child: Column(
        children: <Widget>[
          _ingresarEmail(context, bloc),
          // SizedBox( height: size.height * 0.01),
          _ingresarPassword(context, bloc),
          SizedBox(height: size.height * 0.03),
          _botonLogin(context, bloc),
          _forgotenPassword(context)
        ],
      ),
    );
  }

  Widget _ingresarEmail(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          width: size.width * 0.9,
          height: size.height * 0.12,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,  
            cursorColor: Colors.white,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              hintText: 'NUMERO DE PIN o CORREO',
              counterText: snapshot.data,
              counterStyle: TextStyle(color: Colors.white70, fontSize: 10, fontStyle: FontStyle.italic),
              hintStyle: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600,
                  height: 0.8,
                  color: Color.fromRGBO(255, 255, 255, 0.25)),
            ),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }
  Widget _forgotenPassword(BuildContext context){
    return FlatButton(
      
      onPressed: (){
        Navigator.pushNamed(context, 'forgot_password');
      }, 
      child: Text(
        '¿Olvidaste tu contraseña?',
        style: TextStyle(
          color: Colors.white70,

        ),
      )
    );
  }
  Widget _ingresarPassword(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          width: size.width * 0.9,
          height: size.height * 0.05,
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            cursorColor: Colors.white,
            style: TextStyle(color: Colors.white),
            decoration: InputDecoration(
              hintText: 'CONTRASEÑA',
              errorText: snapshot.error,
              errorStyle: TextStyle(color: Colors.white70, fontSize: 10, fontStyle: FontStyle.italic),
              hintStyle: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600,
                  fontStyle: FontStyle.italic,
                  height: 0.8,
                  color: Color.fromRGBO(255, 255, 255, 0.25)),
            ),
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  Widget _botonLogin(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
        stream: bloc.formValidStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Container(
            height: size.height *0.06,
            child: RaisedButton(
              //padding: EdgeInsets.symmetric(),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: size.width * 0.1, vertical: size.height * 0.01),
                child: Text(
                  'INICIAR SESIÓN',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0)),
              elevation: 0.0,
              color: Colors.white,
              textColor: Colors.purple,
              disabledTextColor: Colors.white10,
              onPressed: snapshot.hasData
                  ? () {
                      _login(bloc, context);
                  }
                  : null /**TODO: Implementación boton INICIAR SESION */
              ),
          );
        });
  }


  _login(LoginBloc bloc, BuildContext context) async {  
      try{
        loadings(context);
        Map info = await usuarioProvider.login(bloc.email, bloc.password);
        if (info['status']) {   
          _coins  = info['coins'];
          _points = info['points'];
          _pin    = info['pin'];
          Navigator.pushNamedAndRemoveUntil(context, 'wellcome_page', (Route<dynamic> route) => false);
          bloc.setCoins(_coins);
          bloc.setPoints(_points);
          bloc.setPin(_pin);
        } else {
          Navigator.pop(context);
          mostrarAlerta(context, info['mensaje']);
        }
      } catch(e){
      } 
  }

  Widget _registro(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.13,
      width: size.width * 0.9,
      margin: EdgeInsets.only( top: size.height * 0.02),
      child: Column(
        children: <Widget>[
          _botonRegistro(context),
        ],
      ),
    );
  }

  Widget  _botonRegistro(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          '¿Aún no tienes cuenta?',
          style: TextStyle(
              fontWeight: FontWeight.w300,
              fontSize: 14.0,
              color: Color.fromRGBO(255, 255, 255, 0.6)),
        ),
        FlatButton(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            child: Text(
              'Registrate aquí',
              style: TextStyle(
                  fontWeight: FontWeight.w300,
                  fontSize: 30.0,
                  color: Color.fromRGBO(255, 255, 255, 0.8)),
            ),
            textColor: Colors.white,
            onPressed: () => Navigator.pushNamed(
                context, 'registro')
            ),
      ],
    );
  }

  Widget _iconos(BuildContext context){
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.08,
      // padding: EdgeInsets.only(bottom: size.height * 0.02),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Image.asset(
            'assets/Icono1Login.png',
            width: 90,
          ),
          Image.asset(
            'assets/Icono2Login.png',
            width: 90,
          )
        ],
      ),
    );
  }
}
