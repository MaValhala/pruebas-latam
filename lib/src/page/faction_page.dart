import 'dart:async';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/faction_model.dart';
import 'package:xplora/src/page/simulacro_page.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/providers/simulacro_provider.dart';
import 'package:xplora/src/utils/utils.dart';
import 'package:xplora/src/widgets/fancy_bottombar.dart';

class FactionPage extends StatelessWidget {
  static const _kFontFam = 'MyFlutterApp';

  static const IconData bell_alt =
      const IconData(0xf0f3, fontFamily: _kFontFam); //Campanita//
  static const IconData rocket =
      const IconData(0xe802, fontFamily: _kFontFam); //Personalizado//
  
  var colors = {
    "1": Color.fromRGBO(242, 27, 87, 1),
    "2": Color.fromRGBO(122, 43, 134, 1),
    "3": Color.fromRGBO(250, 188, 17, 1),
    "4": Color.fromRGBO(21, 171, 159, 1),
    "5": Color.fromRGBO(43, 120, 226, 1),
  };
  var miniBaner = {
    "1": "assets/factions/mini_banner/mini_banner_naturales.png",
    "2": "assets/factions/mini_banner/mini_banner_sociales.png",
    "3": "assets/factions/mini_banner/mini_banner_lectura.png",
    "4": "assets/factions/mini_banner/mini_banner_matematicas.png",
    "5": "assets/factions/mini_banner/mini_banner_ingles.png"
  };
  var logoLatam = {
    "1": "assets/factions/logo_colors/logo_color_naturales.png",
    "2": "assets/factions/logo_colors/logo_color_sociales.png",
    "3": "assets/factions/logo_colors/logo_color_lectura.png",
    "4": "assets/factions/logo_colors/logo_color_matematicas.png",
    "5": "assets/factions/logo_colors/logo_color_ingles.png"
  };
  @override
  Widget build(BuildContext context) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    final Faction faction = ModalRoute.of(context).settings.arguments;
    
    return Scaffold(
      body: Container(
        width: size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(faction.background), fit: BoxFit.cover),
        ),
        child: Column(
          children: <Widget>[
            SafeArea(
              child: _appBar(context, faction),
            ),
            _body(context, bloc),
          ],
        ),
      ),
      // bottomNavigationBar: FancyTabBar(
      //   color: faction.color,
      //   back_color: faction.back_color,
      //   faction: faction,
      //   icon_left: bell_alt,
      //   route: '/',
      //   icon_center: rocket,
      // ),
    );
  }

  Widget _appBar(BuildContext context, Faction faction) {
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);
    return Row(
      children: <Widget>[
        Column(
          children: <Widget>[
            _points(context, bloc),
            Container(
              height: size.height * 0.003,
              width: size.width * 0.3825,
              color: Colors.black12,
            ),
            SizedBox(
              height: size.height * 0.049,
            )
          ],
        ),
        Hero(
          tag: faction.name,
          child: Container(
            width: size.width * 0.235,
            child: Image(
              image: AssetImage(miniBaner[bloc.area]),
              fit: BoxFit.contain,
            ),
          ),
        ),
        Column(
          children: <Widget>[
            _coins(context, bloc),
            Container(
              height: size.height * 0.003,
              width: size.width * 0.3825,
              color: Colors.black12,
            ),
            SizedBox(
              height: size.height * 0.049,
            )
          ],
        ),
      ],
    );
  }

  Widget _points(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.pointStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          alignment: Alignment.center,
          margin: EdgeInsets.only(left: 15),
          color: Colors.white,
          height: size.height * 0.1,
          width: size.width * 0.22,
          child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(image: AssetImage('assets/points.png'))),
            alignment: Alignment.center,
            width: size.width * 0.27,
            child: Text(
              '   ' + (snapshot.data.toString().length>3?'999':snapshot.data),
              style: Theme.of(context)
                  .textTheme
                  .subtitle
                  .apply(color: Colors.black38, fontSizeFactor: 1.1),
              textAlign: TextAlign.left,
            ),
          ),
        );
      },
    );
  }

  Widget _coins(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.coinsStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
            alignment: Alignment.center,
            color: Colors.white,
            height: size.height * 0.1,
            width: size.width * 0.22,
            margin: EdgeInsets.only(right: 15),
            child: Container(
              child: Text(
                '    ' + (snapshot.data.toString().length>3?'999':snapshot.data),
                style: Theme.of(context)
                    .textTheme
                    .subtitle
                    .apply(color: Colors.white, fontSizeFactor: 1.1),
                textAlign: TextAlign.right,
              ),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  image:
                      DecorationImage(image: AssetImage('assets/coins.png'))),
              width: size.width * 0.27,
            ));
      },
    );
  }

  Widget _body(BuildContext context, LoginBloc bloc) {
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    return Column(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          height: size.height * 0.03,
        ),
        Container(
          height: size.height * 0.18,
          child: Image(
            image: AssetImage(logoLatam[bloc.area]),
          ),
        ),
        SizedBox(
          height: size.height * 0.06,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 25, right: 25),
          child: Text(
            'Recuerda que para la prueba tipo contrarreloj el tiempo máximo que tienes es de 5 minutos y para la prueba tipo simulacro es de 25 minutos.',
            style: Theme.of(context)
                .textTheme
                .subhead
                .apply(color: Colors.black54, fontSizeFactor: 1.05),
            textAlign: TextAlign.justify,
          ),
        ),
        SizedBox(
          height: size.height * 0.06,
        ),
        _contraReloj(context, bloc),
        SizedBox(
          height: size.height * 0.055,
        ),
        _simulacro(context, bloc)
      ],
    );
  }

  Widget _contraReloj(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.typeStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(color: colors[bloc.area], width: 3),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: Container(
            alignment: Alignment.center,
            height: size.height * 0.09,
            width: size.width * 0.52,
            child: Text(
              'CONTRARRELOJ',
              style: Theme.of(context)
                  .textTheme
                  .title
                  .apply(color: colors[bloc.area]),
            ),
          ),
          onPressed: () {            
              bloc.changeType('2');
              Navigator.pushNamed(context, 'onboarding');
          }
        );
      },
    );
  }

  Widget _simulacro(BuildContext context, LoginBloc bloc) {
    final size = MediaQuery.of(context).size;
    return StreamBuilder(
      stream: bloc.typeStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
          color: colors[bloc.area],
          shape: RoundedRectangleBorder(
            side: BorderSide(color: colors[bloc.area], width: 3),
            borderRadius: new BorderRadius.circular(10.0),
          ),
          child: Container(
            alignment: Alignment.center,
            height: size.height * 0.09,
            width: size.width * 0.52,
            child: Text(
              'SIMULACRO',
              style:
                  Theme.of(context).textTheme.title.apply(color: Colors.white),
            ),
          ),
          onPressed: () {
            bloc.changeType("1");
            Navigator.pushNamed(context, 'onboarding');
          },
        );
      },
    );
  }
}
