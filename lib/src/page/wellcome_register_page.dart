import 'dart:io';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/page/login_page.dart';
import 'package:xplora/src/providers/factions_provider.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/utils/utils.dart';

class PostRegister extends StatefulWidget {

  @override
  _PostRegisterState createState() => _PostRegisterState();
}

class _PostRegisterState extends State<PostRegister> {
  
  FactionProvider provider = new FactionProvider();
  BuildContext cont;
  @override
  Widget build(BuildContext context) {
    cont= context;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child:  SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/PaginaLogin.png'), fit: BoxFit.cover)
          ),
          child: _contenido(context),

        )
      ),
    );
  }

  Future<bool> _onBackPressed() {
    final s = MediaQuery.of(cont).size;
    return showDialog(
        context: cont,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0),
            ),
            title: Column(
              children: <Widget>[
                SizedBox(height: s.height * 0.03),
                Container(
                  height: s.height * 0.16,
                  child: Image(
                    image: AssetImage('assets/LatamColor.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.04),
                Text(
                  '¡Solo un poco más!',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
                // SizedBox(
                //   height: s.height * 0.015,
                // ),
              ],
            ),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                
                Text(
                  'Estás a un paso de completar el registro, ¿Estás seguro que quieres salir?',
                  textAlign: TextAlign.justify,
                  style: Theme.of(context).textTheme.subhead.apply(color: Colors.black54),
                ),
                SizedBox(
                  height: s.height * 0.025,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      color: Colors.purple,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(12.0)),
                      child: Container(
                        alignment: Alignment.center,
                        height: s.height * 0.06,
                        width: s.width * 0.16,
                        child: Text('No',
                            style: Theme.of(context).textTheme.subhead.apply(
                                  color: Colors.white,
                                )),
                      ),
                      onPressed: () => Navigator.pop(context, false),
                    ),
                    // SizedBox(width: 15,),
                    FlatButton(
                      color: Colors.red,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(12.0)),
                      child: Container(
                        alignment: Alignment.center,
                        height: s.height * 0.06,
                        width: s.width * 0.16,
                        child: Text(
                          'Si',
                          style: Theme.of(context).textTheme.subhead.apply(
                                color: Colors.white,
                              ),
                        ),
                      ),
                      onPressed: () => Navigator.pushNamedAndRemoveUntil(context, 'login', (Route<dynamic> route) => false),
                    ),
                  ],
                ),
              ],
            )
          );
        });
  }

  Widget _contenido(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      child: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(), //*Evita el scroll de una pagina*//
        child: Column(
          children: <Widget>[
            SizedBox(height: size.height*0.09,),
            _logo(context),
            SizedBox(height: size.height*0.05,),
            _texto(context),
            SizedBox(height: size.height*0.05,),
            
          ],
        ),
      ),
    );
  }

  Widget _texto(BuildContext context){
    final bloc = ProviderLogin.of(context);
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        Text(
          '¡Su registro se ha completado',
          style: Theme.of(context).textTheme.title.apply(color:Colors.white, fontSizeFactor: 0.96),
          textAlign: TextAlign.center,
        ),
        SizedBox(height: size.height*0.005,),
        Text(
          'exitosamente!',
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline.apply(color:Colors.white, fontSizeFactor: 1.8),
        ),
        SizedBox(height: size.height*0.01,),
        Container(
          color: Colors.white,
          height: size.height*0.002,
          width: size.width*0.5,
        ),
        SizedBox(height: size.height*0.1,),
        _botonEnviarFaction(context, bloc)
      ],
    );

  }

  Widget _logo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.35,
      width: size.width * 0.6,
      margin: EdgeInsets.symmetric(horizontal: size.width * 0.2, vertical: 10),
      alignment: Alignment.center,
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage('assets/LatamLogin.png'),
      )),
    );
  }
  _cargarFaction(LoginBloc bloc, BuildContext context) async {
    provider.cargarData();
    try {
      loadings(context);
      Map info = await provider.obtenerFaction(bloc.pin);
      if (info['status'] && info['faction_id'] != null) {
        final faction = provider.selectedFaction(info['faction_id']);
        bloc.changeFaction(faction);
        bloc.changeName(info['name']);
        bloc.changeSurname(info['sur_name']);
        bloc.changeLastName1(info['last_name1']);
        bloc.changeLastName2(info['last_name2']);
        bloc.changeEmail(info['email']);
        bloc.changePhone(info['cellphone']);
        bloc.changeDocument(info['document']);
        bloc.changeDocumentType(info['documentType']);
        Navigator.pushNamedAndRemoveUntil(
            context, '/', (Route<dynamic> route) => false);
        ;
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    } catch (e) {}
  }

  Widget _botonEnviarFaction(BuildContext context, LoginBloc bloc) {
    return StreamBuilder(
        stream: bloc.factionStream,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return RaisedButton(
              //padding: EdgeInsets.symmetric(),
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 40, vertical: 5),
                child: Text(
                  'CONTINUAR   >',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50.0)),
              elevation: 0.0,
              color: Colors.white,
              textColor: Colors.purple,
              disabledTextColor: Colors.white10,
              onPressed: snapshot.hasData
                  ? () {
                    if(bloc.faction.id!=null){
                      _ok(bloc.pin, bloc.faction.id, context);
                      _cargarFaction(bloc, context);
                    }
                  }
                  : null /**TODO: Implementación boton INICIAR SESION */
              );
        }
    );
  }

  _ok(String pin, String id, BuildContext context) async {
    try{
      loadings(context);
    
      Map<String, dynamic> info = await provider.seleccionarFaction(id,pin );
      print(info);
      if (info['status']) {
        Navigator.pushNamed(context, '/');
      } else {
        Navigator.pop(context);
        mostrarAlerta(context, info['mensaje']);
      }
    }catch(e){

    }
  }
}
