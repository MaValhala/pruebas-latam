import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/faction_model.dart';
import 'package:xplora/src/providers/factions_provider.dart';
import 'package:xplora/src/utils/utils.dart';
import 'tab_item.dart';
import 'package:vector_math/vector_math.dart' as vector;


class FancyTabBar extends StatefulWidget {
  FancyTabBar({
    @required this.color,
    @required this.back_color,
    @required this.faction,

    @required this.route,

    @required this.bloc,
  });
  LoginBloc bloc;
  int color;  
  int back_color;
  Faction faction;

  String route;
  
  @override
  _FancyTabBarState createState() => _FancyTabBarState();
}

class _FancyTabBarState extends State<FancyTabBar>
  
    with TickerProviderStateMixin {
  AnimationController _animationController;
  Tween<double> _positionTween;
  Animation<double> _positionAnimation; 

  AnimationController _fadeOutController;
  Animation<double> _fadeFabOutAnimation;
  Animation<double> _fadeFabInAnimation;

  double fabIconAlpha = 1;
  IconData nextIcon;
  IconData activeIcon = rocket;

  int currentSelected = 1;

  FactionProvider provider;
  static const _kFontFam = 'MyFlutterApp';
  static const IconData rocket      = const IconData(0xe802, fontFamily: _kFontFam);//Personalizado//
  static const IconData cog         = const IconData(0xe800, fontFamily: _kFontFam);//Configuracion//
  static const IconData help = const IconData(0xe801, fontFamily: _kFontFam);//Signo Pregunta//

  @override
  void initState() {
    
    super.initState();

    _animationController = AnimationController(
        vsync: this, duration: Duration(milliseconds: ANIM_DURATION));
    _fadeOutController = AnimationController(
        vsync: this, duration: Duration(milliseconds: (ANIM_DURATION ~/ 5)));

    _positionTween = Tween<double>(begin: 0, end: 0);
    _positionAnimation = _positionTween.animate(
        CurvedAnimation(parent: _animationController, curve: Curves.easeOut))
      ..addListener(() {
        setState(() {});
      });

    _fadeFabOutAnimation = Tween<double>(begin: 1, end: 0).animate(
        CurvedAnimation(parent: _fadeOutController, curve: Curves.easeOut))
      ..addListener(() {
        setState(() {
          fabIconAlpha = _fadeFabOutAnimation.value;
        });
      })
      ..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed) {
          setState(() {

            activeIcon = nextIcon;
          });
        }
      });

    _fadeFabInAnimation = Tween<double>(begin: 0, end: 1).animate(
        CurvedAnimation(
            parent: _animationController,
            curve: Interval(0.8, 1, curve: Curves.easeOut)))
      ..addListener(() {
        setState(() {
          fabIconAlpha = _fadeFabInAnimation.value;
        });
      });
  }
  
  Widget _enviarFaction (LoginBloc bloc, Faction faction) {

    return StreamBuilder(
      stream: bloc.factionStream ,
      builder: (BuildContext context, AsyncSnapshot snapshot){
        return TabItem(
          selected: currentSelected == 1,
          iconData: rocket,
          title: "",
          color: 0xffffffff,
          callbackFunction: () {
            setState(() {
              nextIcon = rocket;
              currentSelected = 1;
            });
            // mostrarAlerta(context, 'Seleccion de Facción');
            bloc.changeFaction(faction);
            print(bloc.faction.id);
            _initAnimationAndStart(_positionAnimation.value, 0);
            Navigator.pushReplacementNamed(context,'post_register');
          }
        );
      },
    );

  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.topCenter,
      children: <Widget>[
        Container(
          height: 65,
          margin: EdgeInsets.only(top: 45),
          decoration: BoxDecoration(color: Color(widget.color), boxShadow: [
            BoxShadow(
                color: Colors.black12, offset: Offset(0, -1), blurRadius: 8)
          ]),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TabItem(
                  selected: currentSelected == 0,
                  iconData: help,
                  title: "",
                  color: 0xffffffff,
                  callbackFunction: () {
                    setState(() {
                      nextIcon = help;
                      currentSelected = 0;
                    });
                    mostrarAlerta2(context);
                    _initAnimationAndStart(_positionAnimation.value, -1);
                  }),
                  _enviarFaction(widget.bloc, widget.faction),
              // TabItem(
              //     selected: currentSelected == 1,
              //     iconData: rocket,
              //     title: "",
              //     color: 0xffffffff,
              //     callbackFunction: () {
              //       setState(() {
              //         nextIcon = rocket;
              //         currentSelected = 1;
              //       });
              //       // mostrarAlerta(context, 'Seleccion de Facción');
              //       _initAnimationAndStart(_positionAnimation.value, 0);
              //     }),
              TabItem(
                  selected: currentSelected == 2,
                  iconData: cog,
                  color: 0xffffffff,
                  title: "",
                  callbackFunction: () {
                    setState(() {
                      nextIcon = cog;
                      currentSelected = 2;
                    });
                    _initAnimationAndStart(_positionAnimation.value, 1);
                  })
            ],
          ),
        ),
        IgnorePointer(
          child: Container(
            decoration: BoxDecoration(color: Colors.transparent),
            child: Align(
              heightFactor: 1,
              alignment: Alignment(_positionAnimation.value, 0),
              child: FractionallySizedBox(
                widthFactor: 1 / 3,
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    SizedBox(
                      height: 90,
                      width: 90,
                      child: ClipRect(
                          clipper: HalfClipper(),
                          child: Container(
                            child: Center(
                              child: Container(
                                  width: 70,
                                  height: 70,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.circle,
                                      boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 8)])
                              ),
                            ),
                          )),
                    ),
                    SizedBox(
                        height: 70,
                        width: 90,
                        child: CustomPaint(
                          painter: HalfPainter(widget.color),
                        )),
                    SizedBox(
                      height: 60,
                      width: 60,
                      child: Container(
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color(widget.back_color),
                            border: Border.all(
                                color: Colors.white,
                                width: 5,
                                style: BorderStyle.none)),
                        child: Padding(
                          padding: const EdgeInsets.all(0.0),
                          child: Opacity(
                            opacity: fabIconAlpha,
                            child: Icon(
                              activeIcon,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  _initAnimationAndStart(double from, double to) {
    _positionTween.begin = from;
    _positionTween.end = to;

    _animationController.reset();
    _fadeOutController.reset();
    _animationController.forward();
    _fadeOutController.forward();
  }
}

class HalfClipper extends CustomClipper<Rect> {
  @override
  Rect getClip(Size size) {
    final rect = Rect.fromLTWH(0, 0, size.width, size.height / 2);
    return rect;
  }

  @override
  bool shouldReclip(CustomClipper<Rect> oldClipper) {
    return true;
  }
}

class HalfPainter extends CustomPainter {
  HalfPainter(
    @required this.color,
  );
  int color;
  @override
  void paint(Canvas canvas, Size size) {
    final Rect beforeRect = Rect.fromLTWH(0, (size.height / 2) - 10, 10, 10);
    final Rect largeRect = Rect.fromLTWH(10, 0, size.width - 20, 70);
    final Rect afterRect =
        Rect.fromLTWH(size.width - 10, (size.height / 2) - 10, 10, 10);

    final path = Path();
    path.arcTo(beforeRect, vector.radians(0), vector.radians(90), false);
    path.lineTo(20, size.height / 2);
    path.arcTo(largeRect, vector.radians(0), -vector.radians(180), false);
    path.moveTo(size.width - 10, size.height / 2);
    path.lineTo(size.width - 10, (size.height / 2) - 10);
    path.arcTo(afterRect, vector.radians(180), vector.radians(-90), false);
    path.close();

    canvas.drawPath(path, Paint()..color =  Color(color));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}