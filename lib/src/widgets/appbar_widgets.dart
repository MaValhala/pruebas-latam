import 'package:flutter/material.dart';
Widget icono_color(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      children: <Widget>[
        SafeArea(
          child: Container(height: 20,),
        ),
        Container(
          width: size.width * 0.40,
          height: size.height * 0.15,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/pruebasLatam.png'))),
        ),
      ],
    );
  }