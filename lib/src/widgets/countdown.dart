import 'dart:async';
import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/simulacro_model.dart';
import 'package:xplora/src/providers/simulacro_provider.dart';
import 'package:xplora/src/utils/utils.dart';

import 'aux_countdow.dart';

class AdvCountdown extends StatefulWidget {
  final DateTime futureDate;
  final String type;
  final TextStyle style = TextStyle(
      color: Colors.black, fontSize: 25.0, fontWeight: FontWeight.w600);
  AdvCountdown({this.futureDate, this.type, });
  @override
  State<StatefulWidget> createState() => AdvCountdownState();
}

class AdvCountdownState extends State<AdvCountdown> {
  TextStyle _numberStyle;
  TextStyle _datepartStyle;
  String _type;
  Timer _timer;
 
  @override
  void initState() {
    super.initState();
    _type = widget.type;
    _numberStyle = widget.style.copyWith(fontWeight: FontWeight.w700);
    _datepartStyle = widget.style;
    _timer = Timer.periodic(Duration(seconds: 1), (_) {
      if (this.mounted) setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }
  bool endContrarreloj = false;
  SimulacroModel simulacro;
  LoginBloc bloc;
  SimulacroProvider simulacroProvider = new SimulacroProvider();
  @override
  Widget build(BuildContext context) {
    // print(widget.futureDate);
    Duration duration = widget.futureDate.difference(DateTime.now());

    int hour = (duration.inHours.abs() % 24);
    int second = (duration.inSeconds % 60);
    int minute = (duration.inMinutes % 60);
    // int hour = (duration.inHours.abs() % 24);
    // bool extraTime = false;
    String hourDatePart = hour == 1 ? "hora" : "horas";
    
    String minuteDatePart = minute.abs() == 1 ? "minuto" : "minutos";
    String secondDatePart = second.abs() == 1 ? "segundo" : "segundos";
    Widget separator = Text(":", style: _numberStyle);
    List<Widget> children = [];

    if (_type == "1") {
      
        children.addAll([
          separator,
          _buildItem(60-minute>=1&&minute>25?60-second:second, secondDatePart, minute == 0
                    ? Color.fromRGBO(175, 56, 111, 1)
                    : Color.fromRGBO(122, 42, 141, 1))
        ]);
        children.insert(
            0,
            _buildItem(
                60-minute>=1&&minute>25?59-minute:minute,
                minuteDatePart,
                minute == 0
                    ? Color.fromRGBO(175, 56, 111, 1)
                    : Color.fromRGBO(122, 42, 141, 1)));
      
      
    } else {
      if (_type == "2") {
        
          children.addAll([
            separator,
            _buildItem(
                minute==0?60-second==60?0:60-second:second,
                secondDatePart,
                minute == 0
                    ? Color.fromRGBO(175, 56, 111, 1)
                    : Color.fromRGBO(122, 42, 141, 1))
          ]);

          children.insert(
              0,
              _buildItem(
                  minute==0?minute:minute>6?59-minute:minute,
                  minuteDatePart,
                  minute == 0
                      ? Color.fromRGBO(175, 56, 111, 1)
                      : Color.fromRGBO(122, 42, 141, 1)));
      } else {
        if(_type=="3"){
          
              children.insert(0, _buildItem(4-hour<0?(5-hour).abs():4-hour, hourDatePart,minute == 0&& 4-hour<0
                      ? Color.fromRGBO(175, 56, 111, 1)
                      : Color.fromRGBO(122, 42, 141, 1)));
              
            
          children.insert(
              1,
              _buildItem(
                  4-hour>0?minute==60?0:minute:4-hour==0?minute==60?0:minute:4-hour<0?60-minute==60?0:60-minute:minute==60?0:minute,
                  minuteDatePart,
                  minute == 0&& 4-hour<0
                      ? Color.fromRGBO(175, 56, 111, 1)
                      : Color.fromRGBO(122, 42, 141, 1)));
                      children.insert(1, separator);
          children.addAll([
            separator,
            _buildItem(
                4-hour>0?second:4-hour==0?second:4-hour<0?60-second:second,
                secondDatePart,
                minute == 0&& 4-hour<0
                    ? Color.fromRGBO(175, 56, 111, 1)
                    : Color.fromRGBO(122, 42, 141, 1))
          ]);
          }
          

          
        }
      }
    
    // if (19 -hour != 0) {
    //   children.insert(0, separator);
    //   children.insert(0, _buildItem(20-hour, hourDatePart, minute==0&&hour==0?Color.fromRGBO(175,56, 111, 1):Color.fromRGBO(122, 42, 141, 1)));

    // }
    return AdvRow(
        mainAxisSize: MainAxisSize.min,
        divider: RowDivider(5.0),
        crossAxisAlignment: CrossAxisAlignment.start,
        children: children);
  }

  Widget _buildItem(int amount, String datePart, Color color) {
    List<Widget> children = [];
    children.add(Text("$amount", style: TextStyle(color: color, fontSize: 25)));
    children.add(
        Text("$datePart", style: TextStyle(color: color, fontSize: 25 * 0.5)));
    return AdvColumn(children: children);
  }
  
}
