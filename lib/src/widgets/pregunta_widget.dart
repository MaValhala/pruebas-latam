import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/simulacro_model.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:html2md/html2md.dart' as html2md;
import 'package:flutter_markdown/flutter_markdown.dart';
// import 'package:flutter_html_widget/flutter_html_widget.dart';
// import 'package:flutter_html_view/flutter_html_view.dart';

class simulacrosPageView extends StatefulWidget {
  final SimulacroModel simulacro;
  final Function siguientePagina;

  simulacrosPageView(
      {@required this.simulacro, @required this.siguientePagina});

  @override
  _simulacrosPageViewState createState() => _simulacrosPageViewState();
}

class _simulacrosPageViewState extends State<simulacrosPageView> {
  // int _currentIndex=0;

  int respuestaSeleccionada;

  final _pageController =
      new PageController(initialPage: 0, viewportFraction: 1, keepPage: true);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Column(
      children: <Widget>[
        Container(
          height: size.height * 0.79,
          child: PageView(
            physics: BouncingScrollPhysics(),
            children: _cards(context, widget.simulacro),
            pageSnapping: true,
            controller: _pageController,
          ),
        ),
      ],
    );
  }

  List<Widget> _cards(BuildContext context, SimulacroModel simulacro) {
    // _currentIndex=i+1;
    final size = MediaQuery.of(context).size;
    final bloc = ProviderLogin.of(context);

    return simulacro.questions.map((item) {
      String markdown = html2md.convert(item.content);
      return ListView(
        // mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: size.height*0.025,),
          Container(
              padding: EdgeInsets.all(size.height * 0.02),
              alignment: Alignment.center,
              child: new MarkdownBody(
                data: markdown.replaceAll('&nbsp;', ' ').replaceAll('**', '').replaceAll('&amp', '&').replaceAll('&quot', ''),               
                // styleSheet: MarkdownStyleSheet(
                // ),
              )
          ),
          SizedBox(height: size.height*0.02,),
          Column(
            children: _options(context, item, bloc),
          )
          ,SizedBox(height: size.height*0.04,),
        ],
      );
    }).toList();
  }

  List<Widget> _options(
      BuildContext context, Question options, LoginBloc bloc) {
    int x = 0;

    return options.answers.map((option) {
      x++;
      return RadioListTile(
          value: int.parse(option.id),
          title: Text(
            option.content.replaceAll('<sub>', '').replaceAll('</sub>', '').replaceAll('<p>', '').replaceAll('</p>', '').replaceAll('<br>', ''),
          ),
          groupValue: options.selectedAnswer,
          activeColor: Color(bloc.faction.back_color),
          onChanged: (int value) {
            setState(() {
              options.selectedAnswer = value;
            });
          });
    }).toList();
  }
}
