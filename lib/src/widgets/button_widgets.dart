import 'package:flutter/material.dart';

Widget backButtom(BuildContext context, String route) {
    final size = MediaQuery.of(context).size;
    return FlatButton(
      child: Container(
          height: size.height * 0.05,
          width: size.width * 0.25,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              gradient: LinearGradient(
                colors: [
                  Color.fromRGBO(122, 42, 141, 1),
                  Color.fromRGBO(237, 74, 76, 1)
                ],
              )
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.arrow_back_ios,
                color: Colors.white,
                size: size.width*0.035,
              ),
              Text('  ATRÁS',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.white
                ),
              ),
              SizedBox(width: 15,)
            ],
          ),
      ),
      onPressed: () {
        Navigator.pushNamed(context, route);
      },
    );
}

  Widget bottomLogo(final size){
    return Container(
      height: size.height*0.05,
      child: Image(
      image: AssetImage('assets/latam/bottomLogo.png'),
      fit: BoxFit.cover,
    ),
    );
  }