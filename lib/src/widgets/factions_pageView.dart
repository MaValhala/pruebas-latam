
import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:xplora/src/models/faction_model.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:xplora/src/widgets/fancy_bottombar.dart';


class FactionPageView extends StatelessWidget {
  final _pageController = new PageController(
    initialPage: 1,
    viewportFraction: 1,
    keepPage: true,

  );

  static const _kFontFam = 'MyFlutterApp';
  
  static const IconData help = const IconData(0xe801, fontFamily: _kFontFam);//Signo Pregunta//
  static const IconData bell_alt = const IconData(0xf0f3, fontFamily: _kFontFam);//Campanita//
  static const IconData rocket = const IconData(0xe802, fontFamily: _kFontFam);//Personalizado//


  FactionPageView({@required this.factions});
  final List<Faction> factions;
  @override
  Widget build(BuildContext context) {
    
    final bloc = ProviderLogin.of(context);
    return Container(
      child: PageView(
        children: _cards(context, bloc),
        pageSnapping: true,

        // itemCount: factions.length,
        // itemBuilder: (context, i) {
        //   return _card(context, factions[i]);
        // },
        controller: _pageController,
      ),
    );
    
  }
  List<Widget> _cards(BuildContext context, LoginBloc bloc) {
    final size= MediaQuery.of(context).size;
    
    return factions.map((faction){
      final color = Color(faction.color);
      return Scaffold(

        body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          image: DecorationImage(
              image: AssetImage(faction.background),
              fit: BoxFit.cover,
            ),
        ),
        child: Column(
          
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            
            PreferredSize(
            preferredSize: Size.fromHeight(size.height * 0.13),
            child: SafeArea(
              child: Container(
                color: Colors.white,
                  alignment: Alignment.center,
                  height: size.height * 0.13,
                  child: Padding(
                    padding: EdgeInsets.only(top: 13, bottom: 13),
                    child: Image(
                      image: AssetImage('assets/factions/appbar.png'),
                      fit: BoxFit.cover,
                    ),
                  )
              ),
            )
            ),
            Container(
              alignment: Alignment.center,
              width: size.width,
              height: size.height*0.085,
              color: color,
              child: Text(
                faction.name,
                style: Theme.of(context).textTheme.title.apply(color: Colors.white,),
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(height: size.height*0.03,),
            Hero(
              tag: faction.name,
              child: Container(
                width: size.width,
                height: size.height*0.54,
                child: Image(
                  image: AssetImage(faction.banner),
                  fit: BoxFit.contain,
                ),
              ),
            ),

            

          ],
        )
      ),
        bottomNavigationBar: FancyTabBar(color: faction.color,back_color: faction.back_color,faction: faction,route: 'faction_page',bloc : bloc),
      );

    }).toList();
  }
}

