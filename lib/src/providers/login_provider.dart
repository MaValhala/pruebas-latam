import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:xplora/src/bloc/login_bloc.dart';
import 'package:http/http.dart' as http;


class ProviderLogin extends InheritedWidget{


  static ProviderLogin _instancia;
  //TODO: Permite que se deshabilite el boton de inicio de sesion, así mismo, permite que los datos queden guardados al hacer un hotreload
  factory ProviderLogin({Key key, Widget child}){

    if ( _instancia == null ){
      _instancia = new ProviderLogin._internal( key: key, child: child );
    }
    return _instancia;
  }

  ProviderLogin._internal({ Key key, Widget child})
    : super(key: key, child: child);



  final loginBloc = LoginBloc();


  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static LoginBloc of ( BuildContext context ){
    return ( context.inheritFromWidgetOfExactType(ProviderLogin) as ProviderLogin).loginBloc;
  }
}


//TODO: Implementación metodo de logueo 
class UsuarioProvider{

  
  final String _url =  'https://pruebaslatam.com/App';

  Future<Map<String, dynamic>> login(String email, String password) async{

    final authData = {
      'username'   : email,
      'password'   : password,
    };

    final resp = await http.post(
      '$_url/Auth/Login',
      body: authData 
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );
    if ( decodedResp.containsKey('pin')){
      //TODO: Verifica si el usuario se puede loguear
      return { 
        'status': true,
        'coins' : decodedResp['coins'],
        'points': decodedResp['points'],
        'pin'   : decodedResp['pin']
      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }

  }
  Future<Map<String, dynamic>> version() async{


    final resp = await http.get(
      '$_url/Auth/version',
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );
    if ( decodedResp.containsKey('version')){
      //TODO: Verifica si el usuario se puede loguear
      return { 
        'status': true,
        'version' : decodedResp['version'],

      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }

  }

  Future<Map<String, dynamic>> recuperarPass(String email) async{
    
    final authData = {
      'email'   : email,
    };

    final resp = await http.post(
      '$_url/Auth/Recover',
      body: authData 
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );
    if ( decodedResp['message']=='success'){
      //TODO: Verifica si el usuario se puede loguear
      return { 
        'status': true
      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }

  }



}