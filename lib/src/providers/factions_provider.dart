


import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:xplora/src/models/faction_model.dart';


final String _url =  'https://pruebaslatam.com/App';


class FactionProvider{

  

  Future<Map<String, dynamic>> obtenerFaction(String pin) async{
    
    // Faction faction = _selectedFaction(int.parse(id));

    final authData = {
      'pin'          : pin,
    };

    final resp = await http.post(
      '$_url/User_data/find',
      body: authData 
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );
    if ( decodedResp.containsKey('first_name')){
      //TODO: Verifica si el usuario se puede loguear
      return { 
        'status': true,
        'faction_id': decodedResp['faction_id'],
        'name'      : decodedResp['first_name'],
        'sur_name'  : decodedResp['second_name'],
        'last_name1': decodedResp['first_lastname'],
        'last_name2': decodedResp['second_lastname'],
        'email'     : decodedResp['email'],
        'cellphone' : decodedResp['cellphone'],
        'document'  : decodedResp['document_number'],
        'documentType' : decodedResp['user_document_id']
      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }

  }

  Future<Map<String, dynamic>> seleccionarFaction(String id, String pin) async{

    final authData = {
      'faction_id'   : id,
      'pin'          : pin,
    };

    final resp = await http.post(
      '$_url/Student/Update_faction',
      body: authData 
    );

    Map<String, dynamic> decodedResp = json.decode( resp.body );
    if ( decodedResp.containsKey('bool')){
      //TODO: Verifica si el usuario se puede loguear
      return { 
        'status': true,
      };
    } else {
      return { 'status': false, 'mensaje': decodedResp['error']};
    }

  }

  List<Faction> factionss;

  Future<List<Faction>> cargarData() async {
    
    final resp = await rootBundle.loadString('data/factions/factions.json');
      
    final factionsMap = json.decode(resp);
    
    final factions = new Factions.fromJsonList(factionsMap['factions']);
    factionss = factions.factions;
    return factions.factions;
  }
  Faction selectedFaction(String id){
      
     for(var item in factionss){
       if(item.id== id){
         return item;
       }
     }

   }
}