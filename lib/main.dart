import 'package:flutter/services.dart';
import 'package:xplora/src/page/faction_page.dart';
import 'package:xplora/src/page/factions_selection_page.dart';
import 'package:xplora/src/page/feedback_page.dart';
import 'package:xplora/src/page/forgottenPassword_page.dart';
import 'package:xplora/src/page/home_page.dart';
import 'package:flutter/material.dart';
import 'package:xplora/src/page/login_page.dart';
import 'package:xplora/src/page/onboarding_page.dart';
import 'package:xplora/src/page/preicfes_pruebaprogramada_page.dart';
import 'package:xplora/src/page/profile_page.dart';
import 'package:xplora/src/page/recovery_page.dart';
import 'package:xplora/src/page/registro_page.dart';
import 'package:xplora/src/page/revision_details_page.dart';
import 'package:xplora/src/page/revision_page.dart';
import 'package:xplora/src/page/simulacro_page.dart';
import 'package:xplora/src/page/updateProfile_page.dart';
import 'package:xplora/src/page/wellcome_page.dart';
import 'package:xplora/src/page/wellcome_register_page.dart';
// import 'package:xplora/src/page/time_page.dart';
import 'package:xplora/src/providers/login_provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return ProviderLogin(
        child: MaterialApp(
          localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: [
            const Locale('en'), // English
            const Locale('es'),
          ], // Hebrew
          debugShowCheckedModeBanner: false,
          title: 'Pruebas-Latam',
          initialRoute: 'login',
          routes: {
            '/'                : (BuildContext context) => HomePage(),
            // 'timeSelectio   n' : (BuildContext context) => TimeSelectionPage(),
            'simulacro'        : (BuildContext context) => SimulacroPage(),
            'feedback'         : (BuildContext context) => FeedbackPage(),
            'login'            : (BuildContext context) => LoginPage(),
            'registro'         : (BuildContext context) => RegistroPage(),
            'factions'         : (BuildContext context) => FactionsPageSelection(),
            'faction_page'     : (BuildContext context) => FactionPage(),
            'post_register'    : (BuildContext context) => PostRegister(),
            'wellcome_page'    : (BuildContext context) => WellcomePage(),
            'revision'         : (BuildContext context) => RevisionPage(),
            'profile'          : (BuildContext context) => ProfilePage(),
            'updateProfile'    : (BuildContext context) => UpdateProfile(),
            'revision_details' : (BuildContext context) => RevisionDetailsPage(),
            'other_test'       : (BuildContext context) => OtherTestPage(),
            'onboarding'       : (BuildContext context) => LoadingPage(),
            'forgot_password'  : (BuildContext context) => ForgotPasswordPage(),
            'recovery_page'    : (BuildContext context) => RecoveryPage()
          },
          theme: ThemeData(
            primaryColor: Colors.white,
            focusColor: Colors.purple,
            backgroundColor: Colors.white,
            fontFamily: 'Montserrat',
            ),
        )
    );
  }
}
